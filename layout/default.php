<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Aplikasi Clearance Kapal Kantor Imigrasi Kelas II Cirebon</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/DataTables/datatables.min.css">
</head>

<body>
    <div class="header">
        <img src="assets/img/logo.png" class="logo">
        <h1>APLIKASI CLEARANCE KAPAL</h1>
        <h2>KANTOR IMIGRASI KELAS II CIREBON</h2>
    </div>
    
    <div class="container">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Beranda</a></li>
                    <li><a href="index.php?halaman=pegawai">Daftar Pegawai</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Daftar Kapal <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=kapal_dn">Kapal Dalam Negeri</a></li>
                            <li><a href="index.php?halaman=kapal_ln">Kapal Luar Negeri</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Laporan <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=laporan_kapal_ln">Crew Kapal Luar Negeri</a></li>
                            <li><a href="index.php?halaman=laporan_kapal_dn">Crew Kapal Dalam Negeri</a></li>
                            <li><a href="index.php?halaman=laporan_kegiatan_tpi">Kegiatan Tempat Pemeriksaan Imigrasi</a></li>
                        </ul>
                    </li>
                    <li><a href="index.php?halaman=logout">Logout</a></li>
                </ul>
            </div>
        </nav>
        <div class="inner">
            <?php require_once("halaman/".$file_halaman.".php"); ?>
            
            <div class="footer">
                <p>Copyright &copy 2016 Kantor Imigrasi Kelas II Cirebon.</p>
            </div>
        </div>
    </div>
    
    <!-- js -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/DataTables/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables').DataTable();
        });
    </script>
</body>
</html>