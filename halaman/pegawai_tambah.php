<?php
if(isset($_POST["nama_pegawai"])) {
    $q = mysqli_query($conn, "INSERT INTO pegawai
    VALUES(
        null,
        '$_POST[nip]',
        '$_POST[nama_pegawai]'
    )");
    
    if($q && $q1) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=pegawai&msg=$msg");
}
?>

<h3>Tambah Pegawai</h3>

<form action="" method="post">
   
    <div class="form-group">
        <label for="nip">NIP</label>
        <input type="text" name="nip" class="form-control" id="nip" required>
    </div>
    
    <div class="form-group">
        <label for="nama">Nama Pegawai</label>
        <input type="text" name="nama_pegawai" class="form-control" id="nama" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Tambah</button>
    </div>
    
</form>