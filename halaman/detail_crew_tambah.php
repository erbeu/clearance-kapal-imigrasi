<?php
if(isset($_POST["id_negara"])) {
    $q = mysqli_query($conn, "INSERT INTO detail_crew
        VALUES(
            null,
            '$_GET[id]',
            '$_POST[id_negara]',
            '$_POST[jumlah_crew]'
        )") or die(mysqli_error($conn));
    
    if($q) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_$_GET[ket]_detail&id=$_GET[id]&msg=$msg");
}
?>

<h3>Tambah Crew Kapal <?php echo strtoupper($_GET["ket"]); ?></h3>

<form action="" method="post">
   
    <input type="hidden" name="id_kapal" value="<?php echo $_GET[id]; ?>">
    
    <div class="form-group">
        <label for="id_negara">Kewarganegaraan</label>
        <select name="id_negara" class="form-control" id="id_negara" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM negara");
            while($d = mysqli_fetch_assoc($q)) {
                echo "<option value='$d[id_negara]'>$d[nama_negara]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <label for="jumlah_crew">Jumlah Crew</label>
        <input type="number" name="jumlah_crew" class="form-control" id="jumlah_crew" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Tambah</button>
    </div>
    
</form>