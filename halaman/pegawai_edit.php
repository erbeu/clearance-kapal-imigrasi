<?php
if(isset($_POST["nama_pegawai"])) {
    $q = mysqli_query($conn, "UPDATE pegawai
    SET
    nip = '$_POST[nip]',
    nama_pegawai = '$_POST[nama_pegawai]'
    WHERE id_pegawai = '$id'");
    
    if($q && $q1) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=pegawai&msg=$msg");
}

$query = mysqli_query($conn, "SELECT * FROM pegawai WHERE id_pegawai = '$id'");

$data = mysqli_fetch_assoc($query);
?>

<h3>Edit Pegawai</h3>

<form action="" method="post">
   
    <div class="form-group">
        <label for="nip">NIP</label>
        <input type="text" name="nip" class="form-control" id="nip" value="<?php echo $data["nip"]; ?>" required>
    </div>
    
    <div class="form-group">
        <label for="nama">Nama Pegawai</label>
        <input type="text" name="nama_pegawai" class="form-control" id="nama" value="<?php echo $data["nama_pegawai"]; ?>" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Edit</button>
    </div>
    
</form>