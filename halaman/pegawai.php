<h3>Daftar Pegawai</h3>

<a href="index.php?halaman=pegawai_tambah" class="btn btn-primary">Tambah Pegawai</a>

<br><br>

<?php
if($msg != "") {
    echo "<div class='alert alert-info' role='alert'>$msg</div>";
}
?>

<table class="table" id="dataTables">
    <thead>
        <tr>
            <th>No</th>
            <th>NIP</th>
            <th>Nama Pegawai</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT * FROM pegawai");
        while($d = mysqli_fetch_assoc($q)) {
            echo "<tr>";
                echo "<td>$n</td>";
                echo "<td>$d[nip]</td>";
                echo "<td>$d[nama_pegawai]</td>";
                echo "<td>";
                    echo "<a href='index.php?halaman=pegawai_edit&id=$d[id_pegawai]'>Edit</a>";
                    echo " | ";
                    echo "<a href='index.php?halaman=pegawai_hapus&id=$d[id_pegawai]'>Hapus</a>";
                echo "</td>";
            echo "</tr>";

            $n++;
        }
        ?>
        
    </tbody>
</table>