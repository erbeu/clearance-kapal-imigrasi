<?php
if(isset($_POST["id_negara"])) {
    $q = mysqli_query($conn, "UPDATE detail_crew
        SET
        id_negara = '$_POST[id_negara]',
        jumlah_crew = '$_POST[jumlah_crew]'
        WHERE id_crew = '$id'");
    
    if($q) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_$_GET[ket]_detail&id=$_POST[id_kapal]&msg=$msg");
}

$query = mysqli_query($conn, "SELECT *
    FROM detail_crew
    WHERE id_crew = '$id'");

$data = mysqli_fetch_assoc($query);
?>

<h3>Edit Crew Kapal <?php echo strtoupper($_GET["ket"]); ?></h3>

<form action="" method="post">
   
    <input type="hidden" name="id_kapal" value="<?php echo $data["id_kapal"]; ?>">
    
    <div class="form-group">
        <label for="id_negara">Kewarganegaraan</label>
        <select name="id_negara" class="form-control" id="id_negara" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM negara");
            while($d = mysqli_fetch_assoc($q)) {
                if($data["id_negara"] == $d["id_negara"]) {
                    $value = "selected";
                } else {
                    $value = "";
                }
                echo "<option value=\"$d[id_negara]\" $value>$d[nama_negara]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <label for="jumlah_crew">Jumlah Crew</label>
        <input type="number" name="jumlah_crew" class="form-control" id="jumlah_crew" value="<?php echo $data["jumlah_crew"]; ?>" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Edit</button>
    </div>
    
</form>