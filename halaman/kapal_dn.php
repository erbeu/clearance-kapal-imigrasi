<h3>Kapal Dalam Negeri</h3>

<a href="index.php?halaman=kapal_dn_tambah" class="btn btn-primary">Tambah Kapal Dalam Negeri</a>

<br><br>

<?php
if($msg != "") {
    echo "<div class='alert alert-info' role='alert'>$msg</div>";
}
?>

<table class="table" id="dataTables">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kapal</th>
            <th>Nama Pelabuhan</th>
            <th>Tanggal</th>
            <th>Ket</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT *, kapal.id_kapal
            FROM kapal_dn
            JOIN kapal ON kapal_dn.id_kapal = kapal.id_kapal
            ORDER BY tanggal DESC
            ");
        while($d = mysqli_fetch_assoc($q)) {
            echo "<tr>";
                echo "<td>$n</td>";
                echo "<td>$d[nama_kapal]</td>";
            
                $q_pelabuhan = mysqli_query($conn, "SELECT * FROM pelabuhan WHERE id_pelabuhan = $d[id_pelabuhan]");
                $d_pelabuhan = mysqli_fetch_assoc($q_pelabuhan);
                echo "<td>$d_pelabuhan[nama_pelabuhan]</td>";
                echo "<td>$d[tanggal]</td>";
            
                $q_kota = mysqli_query($conn, "SELECT * FROM kota WHERE id_kota = $d[id_kota]");
                $d_kota = mysqli_fetch_assoc($q_kota);
                if($d["ket_kapal"] == "Kedatangan") {
                    $ket = "$d[ket_kapal] Dari $d_kota[nama_kota]";
                } elseif($d["ket_kapal"] == "Keberangkatan") {
                    $ket = "$d[ket_kapal] Ke $d_kota[nama_kota]";
                }
                echo "<td>$ket</td>";
                echo "<td>";
                    echo "<a href='index.php?halaman=kapal_dn_detail&id=$d[id_kapal]'>Detail</a>";
                    echo " | ";
                    echo "<a href='index.php?halaman=kapal_dn_edit&id=$d[id_kapal]'>Edit</a>";
                    echo " | ";
                    echo "<a href='index.php?halaman=kapal_dn_hapus&id=$d[id_kapal]'>Hapus</a>";
                echo "</td>";
            echo "</tr>";
            $n++;
        }
        ?>
        
    </tbody>
</table>