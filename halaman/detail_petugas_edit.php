<?php
if(isset($_POST["id_pegawai"])) {
    $q = mysqli_query($conn, "UPDATE detail_petugas
        SET id_pegawai = '$_POST[id_pegawai]'
        WHERE id_petugas = '$id'");
    
    if($q) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_$_GET[ket]_detail&id=$_POST[id_kapal]&msg=$msg");
}

$query = mysqli_query($conn, "SELECT *
    FROM detail_petugas
    WHERE id_petugas = '$id'");

$data = mysqli_fetch_assoc($query);
?>

<h3>Edit Petugas Kapal <?php echo strtoupper($_GET["ket"]); ?></h3>

<form action="" method="post">
   
    <input type="hidden" name="id_kapal" value="<?php echo $data["id_kapal"]; ?>">
    
    <div class="form-group">
        <label for="id_pegawai">Nama Petugas</label>
        <select name="id_pegawai" class="form-control" id="id_pegawai" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM pegawai");
            while($d = mysqli_fetch_assoc($q)) {
                if($data["id_pegawai"] == $d["id_pegawai"]) {
                    $value = "selected";
                } else {
                    $value = "";
                }
                echo "<option value=\"$d[id_pegawai]\" $value>$d[nama_pegawai]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Edit</button>
    </div>
    
</form>