<h3>Laporan Kapal Luar Negeri</h3>

<form action="" method="get" class="form-inline" target="_blank">
    
    <input type="hidden" name="halaman" value="laporan_kapal_ln_print">
    <input type="hidden" name="layout" value="print">
    
    <select name="bulan" class="form-control" required>
        <option value="">Pilih Bulan</option>
        
        <?php
        foreach(bulan() as $key => $value) {
            echo "<option value='$key'>$value</option>";
        }
        ?>
        
    </select>
    
    <select name="tahun" class="form-control" required>
        <option value="">Pilih Tahun</option>
        
        <?php
        for($i = 2016; $i <= date("Y"); $i++) {
            echo "<option>$i</option>";
        }
        ?>
        
    </select>
    
    <button type="submit" class="btn btn-primary">Print</button>

</form>
<br>