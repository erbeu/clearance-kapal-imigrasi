<h3>Kapal Luar Negeri</h3>

<a href="index.php?halaman=kapal_ln_tambah" class="btn btn-primary">Tambah Kapal Luar Negeri</a>

<br><br>

<?php
if($msg != "") {
    echo "<div class='alert alert-info' role='alert'>$msg</div>";
}
?>

<table class="table" id="dataTables">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kapal</th>
            <th>Nama Pelabuhan</th>
            <th>Tanggal</th>
            <th>Ket</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT *, kapal.id_kapal
            FROM kapal_ln
            JOIN kapal ON kapal_ln.id_kapal = kapal.id_kapal
            ORDER BY tanggal DESC
            ");
        while($d = mysqli_fetch_assoc($q)) {
            echo "<tr>";
                echo "<td>$n</td>";
                echo "<td>$d[nama_kapal]</td>";
            
                $q_pelabuhan = mysqli_query($conn, "SELECT * FROM pelabuhan WHERE id_pelabuhan = $d[id_pelabuhan]");
                $d_pelabuhan = mysqli_fetch_assoc($q_pelabuhan);
                echo "<td>$d_pelabuhan[nama_pelabuhan]</td>";
                echo "<td>$d[tanggal]</td>";
            
                $q_negara = mysqli_query($conn, "SELECT * FROM negara WHERE id_negara = $d[id_negara]");
                $d_negara = mysqli_fetch_assoc($q_negara);
                if($d["ket_kapal"] == "Kedatangan") {
                    $ket = "$d[ket_kapal] Dari $d_negara[nama_negara]";
                } elseif($d["ket_kapal"] == "Keberangkatan") {
                    $ket = "$d[ket_kapal] Ke $d_negara[nama_negara]";
                }
                echo "<td>$ket</td>";
                echo "<td>";
                    echo "<a href='index.php?halaman=kapal_ln_detail&id=$d[id_kapal]'>Detail</a>";
                    echo " | ";
                    echo "<a href='index.php?halaman=kapal_ln_edit&id=$d[id_kapal]'>Edit</a>";
                    echo " | ";
                    echo "<a href='index.php?halaman=kapal_ln_hapus&id=$d[id_kapal]'>Hapus</a>";
                echo "</td>";
            echo "</tr>";
            $n++;
        }
        ?>
    </tbody>
</table>