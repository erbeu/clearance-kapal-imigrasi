<?php
$j_crew = $j_wna = $j_wni = $j_penp = 0;
$where = $_GET["tahun"]."-".sprintf("%02d", $_GET["bulan"]);
?>

<table width="80%" class="header">
    <tr>
        <td width="15%">
            <img src="assets/img/kumham.jpg" alt="" width="100%">
        </td>
        <td width="75%">
            KEMENTERIAN HUKUM DAN HAK ASASI MANUSIA<br>
            KANTOR WILAYAH JAWA BARAT<br>
            <b>KANTOR IMIGRASI KELAS II CIREBON</b><br>
            Jalan Sultan Ageng Tirtayasa No. 51, Cirebon 45153<br>
            Telpon (0231) 488282, Faksimili (0231) 488284-85<br>
            Email : kanim_cirebon@imigrasi.go.id Laman : www.cirebon.imigrasi.go.id
        </td>
    </tr>
</table>

<hr>

<h3 class="text-center">
    LAPORAN JUMLAH CREW KEDATANGAN DAN KEBERANGKATAN<br>
    KAPAL DALAM NEGERI MENURUT KEBANGSAAN<br>
    BULAN : <?php echo strtoupper(bulan($_GET["bulan"]))." ".$_GET["tahun"]; ?>
</h3>

<br>

<table class="table table-bordered">
    <thead>
        <tr>
            <th rowspan="2" width="5%">No</th>
            <th rowspan="2" width="20%">Tanggal</th>
            <th rowspan="2" width="20%">Nama Kapal</th>
            <th rowspan="2">Kedatangan</th>
            <th colspan="4">Jumlah</th>
        </tr>
        <tr>
            <th width="5%">Crew</th>
            <th width="5%">WNA</th>
            <th width="5%">WNI</th>
            <th width="5%">Penp</th>
        </tr>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT *
            FROM kapal_dn
            JOIN kapal ON kapal.id_kapal = kapal_dn.id_kapal
            WHERE ket_kapal = 'Kedatangan' AND tanggal like '$where-%'
            ");
        
        if(mysqli_num_rows($q) == 0) {
            echo "<tr class='text-center'><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>";
        }
                
        while($d = mysqli_fetch_assoc($q)) {
            echo "<tr>";
                echo "<td>$n</td>";
                echo "<td>".date("d-m-Y", strtotime($d["tanggal"]))."</td>";
                echo "<td>$d[nama_kapal]</td>";
            
                $q_kota = mysqli_query($conn, "SELECT * FROM kota WHERE id_kota = $d[id_kota]");
                $d_kota = mysqli_fetch_assoc($q_kota);
                echo "<td>$d_kota[nama_kota]</td>";
                echo "<td>".($d["wna"] + $d["wni"])."</td>";
                echo "<td>$d[wna]</td>";
                echo "<td>$d[wni]</td>";
                echo "<td>$d[jumlah_penumpang]</td>";
            echo "</tr>";

            $j_crew += ($d["wna"] + $d["wni"]);
            $j_wna += $d["wna"];
            $j_wni += $d["wni"];
            $j_penp += $d["jumlah_penumpang"];

            $n++;
        }
        ?>
    </tbody>
    <tfoot>
        <td colspan="4">Jumlah</td>
        <td><?php echo $j_crew; ?></td>
        <td><?php echo $j_wna; ?></td>
        <td><?php echo $j_wni; ?></td>
        <td><?php echo $j_penp; ?></td>
    </tfoot>
</table>

<br>

<?php $j_crew = $j_wna = $j_wni = $j_penp = 0; ?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th rowspan="2" width="5%">No</th>
            <th rowspan="2" width="20%">Tanggal</th>
            <th rowspan="2" width="20%">Nama Kapal</th>
            <th rowspan="2">Keberangkatan</th>
            <th colspan="4">Jumlah</th>
        </tr>
        <tr>
            <th width="5%">Crew</th>
            <th width="5%">WNA</th>
            <th width="5%">WNI</th>
            <th width="5%">Penp</th>
        </tr>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT *
            FROM kapal_dn
            JOIN kapal ON kapal.id_kapal = kapal_dn.id_kapal
            WHERE ket_kapal = 'Keberangkatan' AND tanggal like '$where-%'
            ");
        
        if(mysqli_num_rows($q) == 0) {
            echo "<tr class='text-center'><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>";
        }
                
        while($d = mysqli_fetch_assoc($q)) {
            echo "<tr>";
                echo "<td>$n</td>";
                echo "<td>".date("d-m-Y", strtotime($d["tanggal"]))."</td>";
                echo "<td>$d[nama_kapal]</td>";
            
                $q_kota = mysqli_query($conn, "SELECT * FROM kota WHERE id_kota = $d[id_kota]");
                $d_kota = mysqli_fetch_assoc($q_kota);
                echo "<td>$d_kota[nama_kota]</td>";
                echo "<td>".($d["wna"] + $d["wni"])."</td>";
                echo "<td>$d[wna]</td>";
                echo "<td>$d[wni]</td>";
                echo "<td>$d[jumlah_penumpang]</td>";
            echo "</tr>";

            $j_crew += ($d["wna"] + $d["wni"]);
            $j_wna += $d["wna"];
            $j_wni += $d["wni"];
            $j_penp += $d["jumlah_penumpang"];

            $n++;
        }
        ?>
        
    </tbody>
    <tfoot>
        <td colspan="4">Jumlah</td>
        <td><?php echo $j_crew; ?></td>
        <td><?php echo $j_wna; ?></td>
        <td><?php echo $j_wni; ?></td>
        <td><?php echo $j_penp; ?></td>
    </tfoot>
</table>

<br>
<br>
<br>

<table width="100%">
    <tr>
        <td width="50%"></td>
        <td width="50%" class="text-center">
            An. Kepala Kantor Imigrasi Kelas II Cirebon<br>
            Kasi Lalintuskim<br>
            <br>
            <br>
            <br>
            Yusak<br>
            NIP. 1208371892371293
        </td>
    </tr>
</table>