<?php
if(isset($_POST["ket_kapal"])) {
    $q = mysqli_query($conn, "INSERT INTO kapal
        VALUES(
            null,
            '$_POST[ket_kapal]',
            '$_POST[nama_kapal]',
            '$_POST[id_pelabuhan]',
            '$_POST[wna]',
            '$_POST[wni]',
            '$_POST[jumlah_penumpang]',
            '$_POST[tanggal]'
        )");
    
    $q1 = mysqli_query($conn, "INSERT INTO kapal_dn
        VALUES(
            null,
            '".mysqli_insert_id($conn)."',
            '$_POST[id_kota]'
        )");
    
    if($q && $q1) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_dn&msg=$msg");
}
?>

<h3>Tambah Kapal Dalam Negeri</h3>

<form action="" method="post">
   
    <div class="form-group">
        <label for="ket_kapal">Keterangan Kapal</label>
        <select name="ket_kapal" class="form-control" id="ket_kapal" required>
            <option value=""></option>
            <option value="Kedatangan">Kedatangan Dari</option>
            <option value="Keberangkatan">Keberangkatan Ke</option>
        </select>
    </div>
    
    <div class="form-group">
        <label for="id_kota">Kota/Kab</label>
        <select name="id_kota" class="form-control" id="id_kota" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM kota");
            while($d = mysqli_fetch_assoc($q)) {
                echo "<option value='$d[id_kota]'>$d[nama_kota]</option>";
            }
            ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" name="tanggal" class="form-control" id="tanggal" required>
    </div>
    
    <div class="form-group">
        <label for="nama_kapal">Nama Kapal</label>
        <input type="text" name="nama_kapal" class="form-control" id="nama_kapal" required>
    </div>
    
    <div class="form-group">
        <label for="id_pelabuhan">Nama Pelabuhan</label>
        <select name="id_pelabuhan" class="form-control" id="id_pelabuhan" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM pelabuhan");
            while($d = mysqli_fetch_assoc($q)) {
                echo "<option value='$d[id_pelabuhan]'>$d[nama_pelabuhan]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <label for="wna">WNA</label>
        <input type="number" name="wna" class="form-control" id="wna" required>
    </div>
    
    <div class="form-group">
        <label for="wni">WNI</label>
        <input type="number" name="wni" class="form-control" id="wni" required>
    </div>
    
    <div class="form-group">
        <label for="jumlah_penumpang">Jumlah Penumpang</label>
        <input type="number" name="jumlah_penumpang" class="form-control" id="jumlah_penumpang" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Tambah</button>
    </div>
    
</form>