<?php
if(isset($_POST["username"])) {
    $q = mysqli_query($conn, "SELECT * FROM admin
        WHERE
        username = '$_POST[username]' AND
        password = '".md5($_POST["username"])."'");
    
    if(mysqli_num_rows($q) == 1) {
        $d = mysqli_fetch_assoc($q);
        $_SESSION["id_admin"] = $d["id_admin"];
        $_SESSION["username"] = $d["username"];
        header("location:index.php");
    } else {
        $error = "Username / Password Salah";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Aplikasi Clearance Kapal Kantor Imigrasi Kelas II Cirebon</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        body {
            font-family: "Segoe UI";
            background: url(assets/img/bg1.jpg);
            background-position: top;
            background-repeat: no-repeat;
        }

        .container {
            margin: 100px auto 0;
            max-width: 350px;
            padding: 20px;
            background-color: rgba(0, 0, 0, .8);
        }

        h1 {
            text-align: center;
            color: #eee;
            letter-spacing: 10px;
        }
        button {
            font-weight: bold;
            letter-spacing: 2px;
        }
    </style>
</head>

<body>
    <div class="container">
       
        <h1>LOGIN</h1>
        
        <?php
        if(isset($error)) {
            echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
        }
        ?>
        
        <form action="" method="post">
           
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">LOGIN</button>
            </div>
            
        </form>
    </div>
</body>
</html>