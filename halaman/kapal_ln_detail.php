<?php
$query = mysqli_query($conn, "SELECT *
    FROM kapal_ln
    JOIN kapal ON kapal_ln.id_kapal = kapal.id_kapal
    WHERE kapal.id_kapal = '".$id."'
    ");

$data = mysqli_fetch_assoc($query);

if($msg != "") {
    echo "<div class='alert alert-info' role='alert'>$msg</div>";
}
?>

<div class="row">
    <div class="col-md-6">
        <h3>Detail Kapal Luar Negeri</h3>
        
        <table class="table table-bordered">
            <tr>
                <td width="40%">Nama Kapal</td>
                <td><?php echo $data["nama_kapal"]; ?></td>
            </tr>
            <tr>
                <td>Nama Pelabuhan</td>
                <td>
                    <?php
                    $q_pelabuhan = mysqli_query($conn, "SELECT * FROM pelabuhan WHERE id_pelabuhan = $data[id_pelabuhan]");
                    $d_pelabuhan = mysqli_fetch_assoc($q_pelabuhan);
                    echo $d_pelabuhan["nama_pelabuhan"];
                    ?>
                </td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td><?php echo $data["tanggal"]; ?></td>
            </tr>
            <tr>
                <td>WNA</td>
                <td><?php echo $data["wna"]; ?></td>
            </tr>
            <tr>
                <td>WNI</td>
                <td><?php echo $data["wni"]; ?></td>
            </tr>
            <tr>
                <td>Jumlah Penumpang</td>
                <td><?php echo $data["jumlah_penumpang"]; ?></td>
            </tr>
            <tr>
                <td>Ket</td>

                <?php
                $q_negara = mysqli_query($conn, "SELECT * FROM negara WHERE id_negara = $data[id_negara]");
                $d_negara = mysqli_fetch_assoc($q_negara);
                
                if($data["ket_kapal"] == "Kedatangan") {
                    $ket = "$data[ket_kapal] Dari $d_negara[nama_negara]";
                } elseif($data["ket_kapal"] == "Keberangkatan") {
                    $ket = "$data[ket_kapal] Ke $d_negara[nama_negara]";
                }
                ?>

                <td><?php echo $ket; ?></td>
            </tr>
            <tr>
                <td>Aksi</td>
                <td>
                    <a href="index.php?halaman=kapal_ln_edit&id=<?php echo $data["id_kapal"]; ?>">Edit</a> | 
                    <a href="index.php?halaman=kapal_ln_hapus&id=<?php echo $data["id_kapal"]; ?>">Hapus</a>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <h3>Detail Petugas Kantor</h3>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIP</th>
                    <th>Nama Pegawai</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $n = 1;
                $q = mysqli_query($conn, "SELECT
                    detail_petugas.id_petugas,
                    detail_petugas.id_kapal,
                    pegawai.nip,
                    pegawai.nama_pegawai
                    FROM detail_petugas
                    JOIN pegawai ON detail_petugas.id_pegawai = pegawai.id_pegawai
                    WHERE detail_petugas.id_kapal = '$id'
                    ");
                while($d = mysqli_fetch_assoc($q)) {
                    echo "<tr>";
                        echo "<td>$n</td>";
                        echo "<td>$d[nip]</td>";
                        echo "<td>$d[nama_pegawai]</td>";
                        echo "<td>";
                            echo "<a href='index.php?halaman=detail_petugas_edit&ket=ln&id=$d[id_petugas]'>Edit</a>";
                            echo " | ";
                            echo "<a href='index.php?halaman=detail_petugas_hapus&ket=ln&id=$d[id_petugas]&id_kapal=$d[id_kapal]'>Hapus</a>";
                        echo "</td>";
                    echo "</tr>";
                    $n++;
                }
                ?>
                
            </tbody>
        </table>
        <a href="index.php?halaman=detail_petugas_tambah&ket=ln&id=<?php echo $data["id_kapal"]; ?>" class="btn btn-primary">Tambah Petugas</a>
    </div>
</div>
