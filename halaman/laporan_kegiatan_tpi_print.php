<?php
$where = $_GET["tahun"]."-".sprintf("%02d", $_GET["bulan"]);
?>

<table width="60%" class="header">
    <tr>
        <td width="15%">
            <img src="assets/img/kumham.jpg" alt="" width="100%">
        </td>
        <td width="75%">
            KEMENTERIAN HUKUM DAN HAK ASASI MANUSIA<br>
            REPUBLIK INDONESIA<br>
            <b>KANTOR IMIGRASI KELAS II CIREBON</b><br>
            Jalan Sultan Ageng Tirtayasa No. 51, Cirebon 45153<br>
            Telpon (0231) 488282, Faksimili (0231) 488284-85<br>
            Email : kanim_cirebon@imigrasi.go.id Laman : www.cirebon.imigrasi.go.id
        </td>
    </tr>
</table>

<hr>

<h3 class="text-center">
    LAPORAN KEGIATAN TEMPAT PEMERIKSAAN IMIGRASI<br>
    PADA KANTOR IMIGRASI KELAS II CIREBON<br>
    BULAN <?php echo strtoupper(bulan($_GET["bulan"]))." ".$_GET["tahun"]; ?>
</h3>

<br>

<table class="table table-bordered">
    <thead>
            <th width="5%">No</th>
            <th width="10%">Tanggal</th>
            <th width="15%">Nama Kapal</th>
            <th width="15%">Pelabuhan</th>
            <th width="20%">Petugas Pemeriksa</th>
            <th width="10%">Jumlah Crew</th>
            <th>Keterangan</th>
    </thead>
    <tbody>
       
        <?php
        $n = 1;
        $q = mysqli_query($conn, "SELECT *, kapal.id_kapal
            FROM kapal
            LEFT JOIN kapal_dn ON kapal_dn.id_kapal = kapal.id_kapal
            LEFT JOIN kapal_ln ON kapal_ln.id_kapal = kapal.id_kapal
            WHERE tanggal like '$where-%'
            ORDER BY tanggal ASC
            ");
        while($d = mysqli_fetch_assoc($q)) {
                echo "<tr>";
                    echo "<td>$n</td>";
                    echo "<td>".date("d-m-Y", strtotime($d["tanggal"]))."</td>";
                    echo "<td>$d[nama_kapal]</td>";
                    
                    $q_pelabuhan = mysqli_query($conn, "SELECT * FROM pelabuhan WHERE id_pelabuhan = $d[id_pelabuhan]");
                    $d_pelabuhan = mysqli_fetch_assoc($q_pelabuhan);
                    echo "<td>$d_pelabuhan[nama_pelabuhan]</td>";
                    echo "<td>";
                        echo "<ol>";
                            $q_petugas = mysqli_query($conn, "SELECT
                                nama_pegawai
                                FROM detail_petugas
                                JOIN pegawai ON pegawai.id_pegawai = detail_petugas.id_pegawai
                                WHERE id_kapal = '$d[id_kapal]'
                                ");
                            while($d_petugas = mysqli_fetch_assoc($q_petugas)) {
                                echo "<li>$d_petugas[nama_pegawai]</li>";
                            }
                        echo "</ol>";
                    echo "</td>";
                    echo "<td>";
                        echo "<ul>";
                            echo "<li>WNA : $d[wna]</li>";
                            echo "<li>WNI : $d[wni]</li>";
                        echo "</ul>";
                    echo "</td>";
                    echo "<td>";
                        if(!empty($d["id_ln"])) {
                            $q_negara = mysqli_query($conn, "SELECT * FROM negara WHERE id_negara = $d[id_negara]");
                            $d_negara = mysqli_fetch_assoc($q_negara);
                            
                            $ket = $d_negara["nama_negara"];
                        } else {
                            $q_kota = mysqli_query($conn, "SELECT * FROM kota WHERE id_kota = $d[id_kota]");
                            $d_kota = mysqli_fetch_assoc($q_kota);
                            
                            $ket = $d_kota["nama_kota"];
                        }

                        if($d["ket_kapal"] == "Kedatangan") {
                            echo "$d[ket_kapal] Dari $ket";
                        } elseif($d["ket_kapal"] == "Keberangkatan") {
                            echo "$d[ket_kapal] Ke $ket";
                        }
                    echo "</td>";
                echo "</tr>";

                $n++;
            }
            ?>
        
    </tbody>
</table>

<br>
<br>
<br>

<table width="100%">
    <tr>
        <td width="50%"></td>
        <td width="50%" class="text-center">
            Cirebon, <?php echo date("d")." ".bulan(date("m"))." ".date("Y"); ?><br>
            An. Kepala Kantor Imigrasi Kelas II Cirebon<br>
            Kasi Lalintuskim<br>
            <br>
            <br>
            <br>
            Yusak<br>
            NIP. 1208371892371293
        </td>
    </tr>
</table>