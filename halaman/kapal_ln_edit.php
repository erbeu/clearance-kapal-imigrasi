<?php
if(isset($_POST["ket_kapal"])) {
    $q = mysqli_query($conn, "UPDATE kapal
        SET
        ket_kapal = '$_POST[ket_kapal]',
        nama_kapal = '$_POST[nama_kapal]',
        id_pelabuhan = '$_POST[id_pelabuhan]',
        wna = '$_POST[wna]',
        wni = '$_POST[wni]',
        jumlah_penumpang = '$_POST[jumlah_penumpang]',
        tanggal = '$_POST[tanggal]'
        WHERE id_kapal = '$id'");
    
    $q1 = mysqli_query($conn, "UPDATE kapal_ln
        SET id_negara = '$_POST[id_negara]'
        WHERE id_kapal = '$id'");
    
    if($q && $q1) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_ln&msg=$msg");
}

$query = mysqli_query($conn, "SELECT
    kapal.*,
    kapal_ln.*
    FROM kapal
    JOIN kapal_ln on kapal.id_kapal = kapal_ln.id_kapal
    WHERE kapal.id_kapal = '$id'
    ");

$data = mysqli_fetch_assoc($query);
?>

<h3>Edit Kapal Luar Negeri</h3>

<form action="" method="post">
   
    <div class="form-group">
        <label for="ket_kapal">Keterangan Kapal</label>
        <select name="ket_kapal" class="form-control" id="ket_kapal" required>
            <option value=""></option>

            <?php
            if($data["ket_kapal"] == "Kedatangan") {
                $ked = "selected";
                $keb = "";
            } else {
                $ked = "";
                $keb = "selected";
            }
            ?>

            <option value="Kedatangan" <?php echo $ked; ?>>Kedatangan Dari</option>
            <option value="Keberangkatan" <?php echo $keb; ?>>Keberangkatan Ke</option>
        </select>
    </div>
    
    <div class="form-group">
        <label for="id_negara">Negara</label>
        <select name="id_negara" class="form-control" id="id_negara" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM negara");
            while($d = mysqli_fetch_assoc($q)) {
                if($data["id_negara"] == $d["id_negara"]) {
                    $value = "selected";
                } else {
                    $value = "";
                }
                echo "<option value='$d[id_negara]' $value>$d[id_negara]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" name="tanggal" class="form-control" id="tanggal" value="<?php echo $data["tanggal"]; ?>" required>
    </div>
    
    <div class="form-group">
        <label for="nama_kapal">Nama Kapal</label>
        <input type="text" name="nama_kapal" class="form-control" id="nama_kapal" value="<?php echo $data["nama_kapal"]; ?>" required>
    </div>
    
    <div class="form-group">
        <label for="id_pelabuhan">Nama Pelabuhan</label>
        <select name="id_pelabuhan" class="form-control" id="id_pelabuhan" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM pelabuhan");
            while($d = mysqli_fetch_assoc($q)) {
                if($data["id_pelabuhan"] == $d["id_pelabuhan"]) {
                    $value = "selected";
                } else {
                    $value = "";
                }
                echo "<option value='$d[id_pelabuhan]' $value>$d[nama_pelabuhan]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <label for="wna">WNA</label>
        <input type="number" name="wna" class="form-control" id="wna" value="<?php echo $data["wna"]; ?>" required>
    </div>
    
    <div class="form-group">
        <label for="wni">WNI</label>
        <input type="number" name="wni" class="form-control" id="wni" value="<?php echo $data["wni"]; ?>" required>
    </div>
    
    <div class="form-group">
        <label for="jumlah_penumpang">Jumlah Penumpang</label>
        <input type="number" name="jumlah_penumpang" class="form-control" id="jumlah_penumpang" value="<?php echo $data["jumlah_penumpang"]; ?>" required>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Edit</button>
    </div>
    
</form>