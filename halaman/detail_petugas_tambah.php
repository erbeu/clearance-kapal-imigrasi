<?php
if(isset($_POST["id_pegawai"])) {
    $q = mysqli_query($conn, "INSERT INTO detail_petugas
        VALUES(
            null,
            '$_POST[id_kapal]',
            '$_POST[id_pegawai]'
        )");
    
    if($q) {
        $msg = "Data Berhasil Disimpan";
    } else {
        $msg = "Data Gagal Disimpan";
    }
    
    header("location:index.php?halaman=kapal_$_GET[ket]_detail&id=$_POST[id_kapal]&msg=$msg");
}
?>

<h3>Tambah Petugas Kapal <?php echo strtoupper($_GET["ket"]); ?></h3>

<form action="" method="post">
   
    <input type="hidden" name="id_kapal" value="<?php echo $_GET["id"]; ?>">
    
    <div class="form-group">
        <label for="id_pegawai">Nama Petugas</label>
        <select name="id_pegawai" class="form-control" id="id_pegawai" required>
            <option value=""></option>

            <?php
            $q = mysqli_query($conn, "SELECT * FROM pegawai");
            while($d = mysqli_fetch_assoc($q)) {
                echo "<option value=\"$d[id_pegawai]\">$d[nama_pegawai]</option>";
            }
            ?>

        </select>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-default">Tambah</button>
    </div>
    
</form>