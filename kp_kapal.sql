/*
Navicat MariaDB Data Transfer

Source Server         : MariaDB
Source Server Version : 100110
Source Host           : localhost:3306
Source Database       : kp_kapal

Target Server Type    : MariaDB
Target Server Version : 100110
File Encoding         : 65001

Date: 2016-11-27 14:38:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- ----------------------------
-- Table structure for detail_crew
-- ----------------------------
DROP TABLE IF EXISTS `detail_crew`;
CREATE TABLE `detail_crew` (
  `id_crew` int(11) NOT NULL AUTO_INCREMENT,
  `id_kapal` int(11) NOT NULL,
  `id_negara` int(11) NOT NULL,
  `jumlah_crew` int(11) NOT NULL,
  PRIMARY KEY (`id_crew`),
  KEY `id_arus` (`id_kapal`),
  KEY `id_negara` (`id_negara`),
  CONSTRAINT `detail_crew_ibfk_1` FOREIGN KEY (`id_kapal`) REFERENCES `kapal` (`id_kapal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_crew_ibfk_2` FOREIGN KEY (`id_negara`) REFERENCES `negara` (`id_negara`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_crew
-- ----------------------------
INSERT INTO `detail_crew` VALUES ('1', '1', '10', '12');
INSERT INTO `detail_crew` VALUES ('2', '1', '100', '9');
INSERT INTO `detail_crew` VALUES ('4', '2', '9', '4');
INSERT INTO `detail_crew` VALUES ('5', '2', '100', '30');
INSERT INTO `detail_crew` VALUES ('7', '3', '17', '6');
INSERT INTO `detail_crew` VALUES ('8', '3', '100', '10');
INSERT INTO `detail_crew` VALUES ('9', '4', '100', '11');
INSERT INTO `detail_crew` VALUES ('10', '5', '11', '2');

-- ----------------------------
-- Table structure for detail_petugas
-- ----------------------------
DROP TABLE IF EXISTS `detail_petugas`;
CREATE TABLE `detail_petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `id_kapal` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_pegawai` (`id_pegawai`),
  KEY `id_arus` (`id_kapal`),
  CONSTRAINT `detail_petugas_ibfk_1` FOREIGN KEY (`id_kapal`) REFERENCES `kapal` (`id_kapal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_petugas_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_petugas
-- ----------------------------
INSERT INTO `detail_petugas` VALUES ('1', '1', '2');
INSERT INTO `detail_petugas` VALUES ('3', '1', '1');
INSERT INTO `detail_petugas` VALUES ('4', '2', '1');
INSERT INTO `detail_petugas` VALUES ('6', '3', '1');
INSERT INTO `detail_petugas` VALUES ('7', '4', '1');
INSERT INTO `detail_petugas` VALUES ('8', '6', '1');
INSERT INTO `detail_petugas` VALUES ('9', '5', '1');
INSERT INTO `detail_petugas` VALUES ('10', '7', '2');
INSERT INTO `detail_petugas` VALUES ('11', '7', '1');

-- ----------------------------
-- Table structure for kapal
-- ----------------------------
DROP TABLE IF EXISTS `kapal`;
CREATE TABLE `kapal` (
  `id_kapal` int(11) NOT NULL AUTO_INCREMENT,
  `ket_kapal` enum('Kedatangan','Keberangkatan') NOT NULL,
  `nama_kapal` varchar(255) NOT NULL,
  `id_pelabuhan` int(11) NOT NULL,
  `wna` int(11) NOT NULL,
  `wni` int(11) NOT NULL,
  `jumlah_penumpang` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_kapal`),
  KEY `id_pelabuhan` (`id_pelabuhan`),
  CONSTRAINT `kapal_ibfk_2` FOREIGN KEY (`id_pelabuhan`) REFERENCES `pelabuhan` (`id_pelabuhan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kapal
-- ----------------------------
INSERT INTO `kapal` VALUES ('1', 'Kedatangan', 'ABC', '1', '0', '0', '2', '2016-09-29');
INSERT INTO `kapal` VALUES ('2', 'Kedatangan', 'DEF', '1', '0', '0', '0', '2016-10-02');
INSERT INTO `kapal` VALUES ('3', 'Kedatangan', 'MT 12WE', '2', '0', '0', '6', '2016-10-02');
INSERT INTO `kapal` VALUES ('4', 'Keberangkatan', 'ABC', '2', '113', '3', '3', '2016-10-03');
INSERT INTO `kapal` VALUES ('5', 'Kedatangan', 'ABC', '1', '0', '0', '2', '2016-11-24');
INSERT INTO `kapal` VALUES ('6', 'Kedatangan', 'asdsad', '1', '12', '33', '1', '2016-11-27');
INSERT INTO `kapal` VALUES ('7', 'Keberangkatan', 'klsdjflksdf', '1', '33', '22', '11', '2016-11-28');

-- ----------------------------
-- Table structure for kapal_dn
-- ----------------------------
DROP TABLE IF EXISTS `kapal_dn`;
CREATE TABLE `kapal_dn` (
  `id_dn` int(11) NOT NULL AUTO_INCREMENT,
  `id_kapal` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  PRIMARY KEY (`id_dn`),
  KEY `id_kota` (`id_kota`),
  KEY `id_arus` (`id_kapal`),
  CONSTRAINT `kapal_dn_ibfk_1` FOREIGN KEY (`id_kapal`) REFERENCES `kapal` (`id_kapal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `kapal_dn_ibfk_2` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kapal_dn
-- ----------------------------
INSERT INTO `kapal_dn` VALUES ('1', '1', '17');
INSERT INTO `kapal_dn` VALUES ('2', '2', '87');
INSERT INTO `kapal_dn` VALUES ('3', '5', '19');
INSERT INTO `kapal_dn` VALUES ('4', '6', '3');

-- ----------------------------
-- Table structure for kapal_ln
-- ----------------------------
DROP TABLE IF EXISTS `kapal_ln`;
CREATE TABLE `kapal_ln` (
  `id_ln` int(11) NOT NULL AUTO_INCREMENT,
  `id_kapal` int(11) NOT NULL,
  `id_negara` int(11) NOT NULL,
  PRIMARY KEY (`id_ln`),
  KEY `id_arus` (`id_kapal`),
  KEY `id_negara` (`id_negara`),
  CONSTRAINT `kapal_ln_ibfk_1` FOREIGN KEY (`id_kapal`) REFERENCES `kapal` (`id_kapal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `kapal_ln_ibfk_2` FOREIGN KEY (`id_negara`) REFERENCES `negara` (`id_negara`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kapal_ln
-- ----------------------------
INSERT INTO `kapal_ln` VALUES ('1', '3', '101');
INSERT INTO `kapal_ln` VALUES ('3', '4', '2');
INSERT INTO `kapal_ln` VALUES ('4', '7', '3');

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(255) NOT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kota
-- ----------------------------
INSERT INTO `kota` VALUES ('1', 'Kabupaten Simeulue');
INSERT INTO `kota` VALUES ('2', 'Kabupaten Aceh Singkil');
INSERT INTO `kota` VALUES ('3', 'Kabupaten Aceh Selatan');
INSERT INTO `kota` VALUES ('4', 'Kabupaten Aceh Tenggara');
INSERT INTO `kota` VALUES ('5', 'Kabupaten Aceh Timur');
INSERT INTO `kota` VALUES ('6', 'Kabupaten Aceh Tengah');
INSERT INTO `kota` VALUES ('7', 'Kabupaten Aceh Barat');
INSERT INTO `kota` VALUES ('8', 'Kabupaten Aceh Besar');
INSERT INTO `kota` VALUES ('9', 'Kabupaten Pidie');
INSERT INTO `kota` VALUES ('10', 'Kabupaten Bireuen');
INSERT INTO `kota` VALUES ('11', 'Kabupaten Aceh Utara');
INSERT INTO `kota` VALUES ('12', 'Kabupaten Aceh Barat Daya');
INSERT INTO `kota` VALUES ('13', 'Kabupaten Gayo Lues');
INSERT INTO `kota` VALUES ('14', 'Kabupaten Aceh Tamiang');
INSERT INTO `kota` VALUES ('15', 'Kabupaten Nagan Raya');
INSERT INTO `kota` VALUES ('16', 'Kabupaten Aceh Jaya');
INSERT INTO `kota` VALUES ('17', 'Kabupaten Bener Meriah');
INSERT INTO `kota` VALUES ('18', 'Kabupaten Pidie Jaya');
INSERT INTO `kota` VALUES ('19', 'Kota Banda Aceh');
INSERT INTO `kota` VALUES ('20', 'Kota Sabang');
INSERT INTO `kota` VALUES ('21', 'Kota Langsa');
INSERT INTO `kota` VALUES ('22', 'Kota Lhokseumawe');
INSERT INTO `kota` VALUES ('23', 'Kota Subulussalam');
INSERT INTO `kota` VALUES ('24', 'Kabupaten Nias');
INSERT INTO `kota` VALUES ('25', 'Kabupaten Mandailing Natal');
INSERT INTO `kota` VALUES ('26', 'Kabupaten Tapanuli Selatan');
INSERT INTO `kota` VALUES ('27', 'Kabupaten Tapanuli Tengah');
INSERT INTO `kota` VALUES ('28', 'Kabupaten Tapanuli Utara');
INSERT INTO `kota` VALUES ('29', 'Kabupaten Toba Samosir');
INSERT INTO `kota` VALUES ('30', 'Kabupaten Labuhan Batu');
INSERT INTO `kota` VALUES ('31', 'Kabupaten Asahan');
INSERT INTO `kota` VALUES ('32', 'Kabupaten Simalungun');
INSERT INTO `kota` VALUES ('33', 'Kabupaten Dairi');
INSERT INTO `kota` VALUES ('34', 'Kabupaten Karo');
INSERT INTO `kota` VALUES ('35', 'Kabupaten Deli Serdang');
INSERT INTO `kota` VALUES ('36', 'Kabupaten Langkat');
INSERT INTO `kota` VALUES ('37', 'Kabupaten Nias Selatan');
INSERT INTO `kota` VALUES ('38', 'Kabupaten Humbang Hasundutan');
INSERT INTO `kota` VALUES ('39', 'Kabupaten Pakpak Bharat');
INSERT INTO `kota` VALUES ('40', 'Kabupaten Samosir');
INSERT INTO `kota` VALUES ('41', 'Kabupaten Serdang Bedagai');
INSERT INTO `kota` VALUES ('42', 'Kabupaten Batu Bara');
INSERT INTO `kota` VALUES ('43', 'Kabupaten Padang Lawas Utara');
INSERT INTO `kota` VALUES ('44', 'Kabupaten Padang Lawas');
INSERT INTO `kota` VALUES ('45', 'Kabupaten Labuhan Batu Selatan');
INSERT INTO `kota` VALUES ('46', 'Kabupaten Labuhan Batu Utara');
INSERT INTO `kota` VALUES ('47', 'Kabupaten Nias Utara');
INSERT INTO `kota` VALUES ('48', 'Kabupaten Nias Barat');
INSERT INTO `kota` VALUES ('49', 'Kota Sibolga');
INSERT INTO `kota` VALUES ('50', 'Kota Tanjung Balai');
INSERT INTO `kota` VALUES ('51', 'Kota Pematang Siantar');
INSERT INTO `kota` VALUES ('52', 'Kota Tebing Tinggi');
INSERT INTO `kota` VALUES ('53', 'Kota Medan');
INSERT INTO `kota` VALUES ('54', 'Kota Binjai');
INSERT INTO `kota` VALUES ('55', 'Kota Padangsidimpuan');
INSERT INTO `kota` VALUES ('56', 'Kota Gunungsitoli');
INSERT INTO `kota` VALUES ('57', 'Kabupaten Kepulauan Mentawai');
INSERT INTO `kota` VALUES ('58', 'Kabupaten Pesisir Selatan');
INSERT INTO `kota` VALUES ('59', 'Kabupaten Solok');
INSERT INTO `kota` VALUES ('60', 'Kabupaten Sijunjung');
INSERT INTO `kota` VALUES ('61', 'Kabupaten Tanah Datar');
INSERT INTO `kota` VALUES ('62', 'Kabupaten Padang Pariaman');
INSERT INTO `kota` VALUES ('63', 'Kabupaten Agam');
INSERT INTO `kota` VALUES ('64', 'Kabupaten Lima Puluh Kota');
INSERT INTO `kota` VALUES ('65', 'Kabupaten Pasaman');
INSERT INTO `kota` VALUES ('66', 'Kabupaten Solok Selatan');
INSERT INTO `kota` VALUES ('67', 'Kabupaten Dharmasraya');
INSERT INTO `kota` VALUES ('68', 'Kabupaten Pasaman Barat');
INSERT INTO `kota` VALUES ('69', 'Kota Padang');
INSERT INTO `kota` VALUES ('70', 'Kota Solok');
INSERT INTO `kota` VALUES ('71', 'Kota Sawah Lunto');
INSERT INTO `kota` VALUES ('72', 'Kota Padang Panjang');
INSERT INTO `kota` VALUES ('73', 'Kota Bukittinggi');
INSERT INTO `kota` VALUES ('74', 'Kota Payakumbuh');
INSERT INTO `kota` VALUES ('75', 'Kota Pariaman');
INSERT INTO `kota` VALUES ('76', 'Kabupaten Kuantan Singingi');
INSERT INTO `kota` VALUES ('77', 'Kabupaten Indragiri Hulu');
INSERT INTO `kota` VALUES ('78', 'Kabupaten Indragiri Hilir');
INSERT INTO `kota` VALUES ('79', 'Kabupaten Pelalawan');
INSERT INTO `kota` VALUES ('80', 'Kabupaten Siak');
INSERT INTO `kota` VALUES ('81', 'Kabupaten Kampar');
INSERT INTO `kota` VALUES ('82', 'Kabupaten Rokan Hulu');
INSERT INTO `kota` VALUES ('83', 'Kabupaten Bengkalis');
INSERT INTO `kota` VALUES ('84', 'Kabupaten Rokan Hilir');
INSERT INTO `kota` VALUES ('85', 'Kabupaten Kepulauan Meranti');
INSERT INTO `kota` VALUES ('86', 'Kota Pekanbaru');
INSERT INTO `kota` VALUES ('87', 'Kota Dumai');
INSERT INTO `kota` VALUES ('88', 'Kabupaten Kerinci');
INSERT INTO `kota` VALUES ('89', 'Kabupaten Merangin');
INSERT INTO `kota` VALUES ('90', 'Kabupaten Sarolangun');
INSERT INTO `kota` VALUES ('91', 'Kabupaten Batang Hari');
INSERT INTO `kota` VALUES ('92', 'Kabupaten Muaro Jambi');
INSERT INTO `kota` VALUES ('93', 'Kabupaten Tanjung Jabung Timur');
INSERT INTO `kota` VALUES ('94', 'Kabupaten Tanjung Jabung Barat');
INSERT INTO `kota` VALUES ('95', 'Kabupaten Tebo');
INSERT INTO `kota` VALUES ('96', 'Kabupaten Bungo');
INSERT INTO `kota` VALUES ('97', 'Kota Jambi');
INSERT INTO `kota` VALUES ('98', 'Kota Sungai Penuh');
INSERT INTO `kota` VALUES ('99', 'Kabupaten Ogan Komering Ulu');
INSERT INTO `kota` VALUES ('100', 'Kabupaten Ogan Komering Ilir');
INSERT INTO `kota` VALUES ('101', 'Kabupaten Muara Enim');
INSERT INTO `kota` VALUES ('102', 'Kabupaten Lahat');
INSERT INTO `kota` VALUES ('103', 'Kabupaten Musi Rawas');
INSERT INTO `kota` VALUES ('104', 'Kabupaten Musi Banyuasin');
INSERT INTO `kota` VALUES ('105', 'Kabupaten Banyu Asin');
INSERT INTO `kota` VALUES ('106', 'Kabupaten Ogan Komering Ulu Selatan');
INSERT INTO `kota` VALUES ('107', 'Kabupaten Ogan Komering Ulu Timur');
INSERT INTO `kota` VALUES ('108', 'Kabupaten Ogan Ilir');
INSERT INTO `kota` VALUES ('109', 'Kabupaten Empat Lawang');
INSERT INTO `kota` VALUES ('110', 'Kabupaten Penukal Abab Lematang Ilir');
INSERT INTO `kota` VALUES ('111', 'Kabupaten Musi Rawas Utara');
INSERT INTO `kota` VALUES ('112', 'Kota Palembang');
INSERT INTO `kota` VALUES ('113', 'Kota Prabumulih');
INSERT INTO `kota` VALUES ('114', 'Kota Pagar Alam');
INSERT INTO `kota` VALUES ('115', 'Kota Lubuklinggau');
INSERT INTO `kota` VALUES ('116', 'Kabupaten Bengkulu Selatan');
INSERT INTO `kota` VALUES ('117', 'Kabupaten Rejang Lebong');
INSERT INTO `kota` VALUES ('118', 'Kabupaten Bengkulu Utara');
INSERT INTO `kota` VALUES ('119', 'Kabupaten Kaur');
INSERT INTO `kota` VALUES ('120', 'Kabupaten Seluma');
INSERT INTO `kota` VALUES ('121', 'Kabupaten Mukomuko');
INSERT INTO `kota` VALUES ('122', 'Kabupaten Lebong');
INSERT INTO `kota` VALUES ('123', 'Kabupaten Kepahiang');
INSERT INTO `kota` VALUES ('124', 'Kabupaten Bengkulu Tengah');
INSERT INTO `kota` VALUES ('125', 'Kota Bengkulu');
INSERT INTO `kota` VALUES ('126', 'Kabupaten Lampung Barat');
INSERT INTO `kota` VALUES ('127', 'Kabupaten Tanggamus');
INSERT INTO `kota` VALUES ('128', 'Kabupaten Lampung Selatan');
INSERT INTO `kota` VALUES ('129', 'Kabupaten Lampung Timur');
INSERT INTO `kota` VALUES ('130', 'Kabupaten Lampung Tengah');
INSERT INTO `kota` VALUES ('131', 'Kabupaten Lampung Utara');
INSERT INTO `kota` VALUES ('132', 'Kabupaten Way Kanan');
INSERT INTO `kota` VALUES ('133', 'Kabupaten Tulangbawang');
INSERT INTO `kota` VALUES ('134', 'Kabupaten Pesawaran');
INSERT INTO `kota` VALUES ('135', 'Kabupaten Pringsewu');
INSERT INTO `kota` VALUES ('136', 'Kabupaten Mesuji');
INSERT INTO `kota` VALUES ('137', 'Kabupaten Tulang Bawang Barat');
INSERT INTO `kota` VALUES ('138', 'Kabupaten Pesisir Barat');
INSERT INTO `kota` VALUES ('139', 'Kota Bandar Lampung');
INSERT INTO `kota` VALUES ('140', 'Kota Metro');
INSERT INTO `kota` VALUES ('141', 'Kabupaten Bangka');
INSERT INTO `kota` VALUES ('142', 'Kabupaten Belitung');
INSERT INTO `kota` VALUES ('143', 'Kabupaten Bangka Barat');
INSERT INTO `kota` VALUES ('144', 'Kabupaten Bangka Tengah');
INSERT INTO `kota` VALUES ('145', 'Kabupaten Bangka Selatan');
INSERT INTO `kota` VALUES ('146', 'Kabupaten Belitung Timur');
INSERT INTO `kota` VALUES ('147', 'Kota Pangkal Pinang');
INSERT INTO `kota` VALUES ('148', 'Kabupaten Karimun');
INSERT INTO `kota` VALUES ('149', 'Kabupaten Bintan');
INSERT INTO `kota` VALUES ('150', 'Kabupaten Natuna');
INSERT INTO `kota` VALUES ('151', 'Kabupaten Lingga');
INSERT INTO `kota` VALUES ('152', 'Kabupaten Kepulauan Anambas');
INSERT INTO `kota` VALUES ('153', 'Kota Batam');
INSERT INTO `kota` VALUES ('154', 'Kota Tanjung Pinang');
INSERT INTO `kota` VALUES ('155', 'Kabupaten Kepulauan Seribu');
INSERT INTO `kota` VALUES ('156', 'Kota Jakarta Selatan');
INSERT INTO `kota` VALUES ('157', 'Kota Jakarta Timur');
INSERT INTO `kota` VALUES ('158', 'Kota Jakarta Pusat');
INSERT INTO `kota` VALUES ('159', 'Kota Jakarta Barat');
INSERT INTO `kota` VALUES ('160', 'Kota Jakarta Utara');
INSERT INTO `kota` VALUES ('161', 'Kabupaten Bogor');
INSERT INTO `kota` VALUES ('162', 'Kabupaten Sukabumi');
INSERT INTO `kota` VALUES ('163', 'Kabupaten Cianjur');
INSERT INTO `kota` VALUES ('164', 'Kabupaten Bandung');
INSERT INTO `kota` VALUES ('165', 'Kabupaten Garut');
INSERT INTO `kota` VALUES ('166', 'Kabupaten Tasikmalaya');
INSERT INTO `kota` VALUES ('167', 'Kabupaten Ciamis');
INSERT INTO `kota` VALUES ('168', 'Kabupaten Kuningan');
INSERT INTO `kota` VALUES ('169', 'Kabupaten Cirebon');
INSERT INTO `kota` VALUES ('170', 'Kabupaten Majalengka');
INSERT INTO `kota` VALUES ('171', 'Kabupaten Sumedang');
INSERT INTO `kota` VALUES ('172', 'Kabupaten Indramayu');
INSERT INTO `kota` VALUES ('173', 'Kabupaten Subang');
INSERT INTO `kota` VALUES ('174', 'Kabupaten Purwakarta');
INSERT INTO `kota` VALUES ('175', 'Kabupaten Karawang');
INSERT INTO `kota` VALUES ('176', 'Kabupaten Bekasi');
INSERT INTO `kota` VALUES ('177', 'Kabupaten Bandung Barat');
INSERT INTO `kota` VALUES ('178', 'Kabupaten Pangandaran');
INSERT INTO `kota` VALUES ('179', 'Kota Bogor');
INSERT INTO `kota` VALUES ('180', 'Kota Sukabumi');
INSERT INTO `kota` VALUES ('181', 'Kota Bandung');
INSERT INTO `kota` VALUES ('182', 'Kota Cirebon');
INSERT INTO `kota` VALUES ('183', 'Kota Bekasi');
INSERT INTO `kota` VALUES ('184', 'Kota Depok');
INSERT INTO `kota` VALUES ('185', 'Kota Cimahi');
INSERT INTO `kota` VALUES ('186', 'Kota Tasikmalaya');
INSERT INTO `kota` VALUES ('187', 'Kota Banjar');
INSERT INTO `kota` VALUES ('188', 'Kabupaten Cilacap');
INSERT INTO `kota` VALUES ('189', 'Kabupaten Banyumas');
INSERT INTO `kota` VALUES ('190', 'Kabupaten Purbalingga');
INSERT INTO `kota` VALUES ('191', 'Kabupaten Banjarnegara');
INSERT INTO `kota` VALUES ('192', 'Kabupaten Kebumen');
INSERT INTO `kota` VALUES ('193', 'Kabupaten Purworejo');
INSERT INTO `kota` VALUES ('194', 'Kabupaten Wonosobo');
INSERT INTO `kota` VALUES ('195', 'Kabupaten Magelang');
INSERT INTO `kota` VALUES ('196', 'Kabupaten Boyolali');
INSERT INTO `kota` VALUES ('197', 'Kabupaten Klaten');
INSERT INTO `kota` VALUES ('198', 'Kabupaten Sukoharjo');
INSERT INTO `kota` VALUES ('199', 'Kabupaten Wonogiri');
INSERT INTO `kota` VALUES ('200', 'Kabupaten Karanganyar');
INSERT INTO `kota` VALUES ('201', 'Kabupaten Sragen');
INSERT INTO `kota` VALUES ('202', 'Kabupaten Grobogan');
INSERT INTO `kota` VALUES ('203', 'Kabupaten Blora');
INSERT INTO `kota` VALUES ('204', 'Kabupaten Rembang');
INSERT INTO `kota` VALUES ('205', 'Kabupaten Pati');
INSERT INTO `kota` VALUES ('206', 'Kabupaten Kudus');
INSERT INTO `kota` VALUES ('207', 'Kabupaten Jepara');
INSERT INTO `kota` VALUES ('208', 'Kabupaten Demak');
INSERT INTO `kota` VALUES ('209', 'Kabupaten Semarang');
INSERT INTO `kota` VALUES ('210', 'Kabupaten Temanggung');
INSERT INTO `kota` VALUES ('211', 'Kabupaten Kendal');
INSERT INTO `kota` VALUES ('212', 'Kabupaten Batang');
INSERT INTO `kota` VALUES ('213', 'Kabupaten Pekalongan');
INSERT INTO `kota` VALUES ('214', 'Kabupaten Pemalang');
INSERT INTO `kota` VALUES ('215', 'Kabupaten Tegal');
INSERT INTO `kota` VALUES ('216', 'Kabupaten Brebes');
INSERT INTO `kota` VALUES ('217', 'Kota Magelang');
INSERT INTO `kota` VALUES ('218', 'Kota Surakarta');
INSERT INTO `kota` VALUES ('219', 'Kota Salatiga');
INSERT INTO `kota` VALUES ('220', 'Kota Semarang');
INSERT INTO `kota` VALUES ('221', 'Kota Pekalongan');
INSERT INTO `kota` VALUES ('222', 'Kota Tegal');
INSERT INTO `kota` VALUES ('223', 'Kabupaten Kulon Progo');
INSERT INTO `kota` VALUES ('224', 'Kabupaten Bantul');
INSERT INTO `kota` VALUES ('225', 'Kabupaten Gunung Kidul');
INSERT INTO `kota` VALUES ('226', 'Kabupaten Sleman');
INSERT INTO `kota` VALUES ('227', 'Kota Yogyakarta');
INSERT INTO `kota` VALUES ('228', 'Kabupaten Pacitan');
INSERT INTO `kota` VALUES ('229', 'Kabupaten Ponorogo');
INSERT INTO `kota` VALUES ('230', 'Kabupaten Trenggalek');
INSERT INTO `kota` VALUES ('231', 'Kabupaten Tulungagung');
INSERT INTO `kota` VALUES ('232', 'Kabupaten Blitar');
INSERT INTO `kota` VALUES ('233', 'Kabupaten Kediri');
INSERT INTO `kota` VALUES ('234', 'Kabupaten Malang');
INSERT INTO `kota` VALUES ('235', 'Kabupaten Lumajang');
INSERT INTO `kota` VALUES ('236', 'Kabupaten Jember');
INSERT INTO `kota` VALUES ('237', 'Kabupaten Banyuwangi');
INSERT INTO `kota` VALUES ('238', 'Kabupaten Bondowoso');
INSERT INTO `kota` VALUES ('239', 'Kabupaten Situbondo');
INSERT INTO `kota` VALUES ('240', 'Kabupaten Probolinggo');
INSERT INTO `kota` VALUES ('241', 'Kabupaten Pasuruan');
INSERT INTO `kota` VALUES ('242', 'Kabupaten Sidoarjo');
INSERT INTO `kota` VALUES ('243', 'Kabupaten Mojokerto');
INSERT INTO `kota` VALUES ('244', 'Kabupaten Jombang');
INSERT INTO `kota` VALUES ('245', 'Kabupaten Nganjuk');
INSERT INTO `kota` VALUES ('246', 'Kabupaten Madiun');
INSERT INTO `kota` VALUES ('247', 'Kabupaten Magetan');
INSERT INTO `kota` VALUES ('248', 'Kabupaten Ngawi');
INSERT INTO `kota` VALUES ('249', 'Kabupaten Bojonegoro');
INSERT INTO `kota` VALUES ('250', 'Kabupaten Tuban');
INSERT INTO `kota` VALUES ('251', 'Kabupaten Lamongan');
INSERT INTO `kota` VALUES ('252', 'Kabupaten Gresik');
INSERT INTO `kota` VALUES ('253', 'Kabupaten Bangkalan');
INSERT INTO `kota` VALUES ('254', 'Kabupaten Sampang');
INSERT INTO `kota` VALUES ('255', 'Kabupaten Pamekasan');
INSERT INTO `kota` VALUES ('256', 'Kabupaten Sumenep');
INSERT INTO `kota` VALUES ('257', 'Kota Kediri');
INSERT INTO `kota` VALUES ('258', 'Kota Blitar');
INSERT INTO `kota` VALUES ('259', 'Kota Malang');
INSERT INTO `kota` VALUES ('260', 'Kota Probolinggo');
INSERT INTO `kota` VALUES ('261', 'Kota Pasuruan');
INSERT INTO `kota` VALUES ('262', 'Kota Mojokerto');
INSERT INTO `kota` VALUES ('263', 'Kota Madiun');
INSERT INTO `kota` VALUES ('264', 'Kota Surabaya');
INSERT INTO `kota` VALUES ('265', 'Kota Batu');
INSERT INTO `kota` VALUES ('266', 'Kabupaten Pandeglang');
INSERT INTO `kota` VALUES ('267', 'Kabupaten Lebak');
INSERT INTO `kota` VALUES ('268', 'Kabupaten Tangerang');
INSERT INTO `kota` VALUES ('269', 'Kabupaten Serang');
INSERT INTO `kota` VALUES ('270', 'Kota Tangerang');
INSERT INTO `kota` VALUES ('271', 'Kota Cilegon');
INSERT INTO `kota` VALUES ('272', 'Kota Serang');
INSERT INTO `kota` VALUES ('273', 'Kota Tangerang Selatan');
INSERT INTO `kota` VALUES ('274', 'Kabupaten Jembrana');
INSERT INTO `kota` VALUES ('275', 'Kabupaten Tabanan');
INSERT INTO `kota` VALUES ('276', 'Kabupaten Badung');
INSERT INTO `kota` VALUES ('277', 'Kabupaten Gianyar');
INSERT INTO `kota` VALUES ('278', 'Kabupaten Klungkung');
INSERT INTO `kota` VALUES ('279', 'Kabupaten Bangli');
INSERT INTO `kota` VALUES ('280', 'Kabupaten Karang Asem');
INSERT INTO `kota` VALUES ('281', 'Kabupaten Buleleng');
INSERT INTO `kota` VALUES ('282', 'Kota Denpasar');
INSERT INTO `kota` VALUES ('283', 'Kabupaten Lombok Barat');
INSERT INTO `kota` VALUES ('284', 'Kabupaten Lombok Tengah');
INSERT INTO `kota` VALUES ('285', 'Kabupaten Lombok Timur');
INSERT INTO `kota` VALUES ('286', 'Kabupaten Sumbawa');
INSERT INTO `kota` VALUES ('287', 'Kabupaten Dompu');
INSERT INTO `kota` VALUES ('288', 'Kabupaten Bima');
INSERT INTO `kota` VALUES ('289', 'Kabupaten Sumbawa Barat');
INSERT INTO `kota` VALUES ('290', 'Kabupaten Lombok Utara');
INSERT INTO `kota` VALUES ('291', 'Kota Mataram');
INSERT INTO `kota` VALUES ('292', 'Kota Bima');
INSERT INTO `kota` VALUES ('293', 'Kabupaten Sumba Barat');
INSERT INTO `kota` VALUES ('294', 'Kabupaten Sumba Timur');
INSERT INTO `kota` VALUES ('295', 'Kabupaten Kupang');
INSERT INTO `kota` VALUES ('296', 'Kabupaten Timor Tengah Selatan');
INSERT INTO `kota` VALUES ('297', 'Kabupaten Timor Tengah Utara');
INSERT INTO `kota` VALUES ('298', 'Kabupaten Belu');
INSERT INTO `kota` VALUES ('299', 'Kabupaten Alor');
INSERT INTO `kota` VALUES ('300', 'Kabupaten Lembata');
INSERT INTO `kota` VALUES ('301', 'Kabupaten Flores Timur');
INSERT INTO `kota` VALUES ('302', 'Kabupaten Sikka');
INSERT INTO `kota` VALUES ('303', 'Kabupaten Ende');
INSERT INTO `kota` VALUES ('304', 'Kabupaten Ngada');
INSERT INTO `kota` VALUES ('305', 'Kabupaten Manggarai');
INSERT INTO `kota` VALUES ('306', 'Kabupaten Rote Ndao');
INSERT INTO `kota` VALUES ('307', 'Kabupaten Manggarai Barat');
INSERT INTO `kota` VALUES ('308', 'Kabupaten Sumba Tengah');
INSERT INTO `kota` VALUES ('309', 'Kabupaten Sumba Barat Daya');
INSERT INTO `kota` VALUES ('310', 'Kabupaten Nagekeo');
INSERT INTO `kota` VALUES ('311', 'Kabupaten Manggarai Timur');
INSERT INTO `kota` VALUES ('312', 'Kabupaten Sabu Raijua');
INSERT INTO `kota` VALUES ('313', 'Kabupaten Malaka');
INSERT INTO `kota` VALUES ('314', 'Kota Kupang');
INSERT INTO `kota` VALUES ('315', 'Kabupaten Sambas');
INSERT INTO `kota` VALUES ('316', 'Kabupaten Bengkayang');
INSERT INTO `kota` VALUES ('317', 'Kabupaten Landak');
INSERT INTO `kota` VALUES ('318', 'Kabupaten Mempawah');
INSERT INTO `kota` VALUES ('319', 'Kabupaten Sanggau');
INSERT INTO `kota` VALUES ('320', 'Kabupaten Ketapang');
INSERT INTO `kota` VALUES ('321', 'Kabupaten Sintang');
INSERT INTO `kota` VALUES ('322', 'Kabupaten Kapuas Hulu');
INSERT INTO `kota` VALUES ('323', 'Kabupaten Sekadau');
INSERT INTO `kota` VALUES ('324', 'Kabupaten Melawi');
INSERT INTO `kota` VALUES ('325', 'Kabupaten Kayong Utara');
INSERT INTO `kota` VALUES ('326', 'Kabupaten Kubu Raya');
INSERT INTO `kota` VALUES ('327', 'Kota Pontianak');
INSERT INTO `kota` VALUES ('328', 'Kota Singkawang');
INSERT INTO `kota` VALUES ('329', 'Kabupaten Kotawaringin Barat');
INSERT INTO `kota` VALUES ('330', 'Kabupaten Kotawaringin Timur');
INSERT INTO `kota` VALUES ('331', 'Kabupaten Kapuas');
INSERT INTO `kota` VALUES ('332', 'Kabupaten Barito Selatan');
INSERT INTO `kota` VALUES ('333', 'Kabupaten Barito Utara');
INSERT INTO `kota` VALUES ('334', 'Kabupaten Sukamara');
INSERT INTO `kota` VALUES ('335', 'Kabupaten Lamandau');
INSERT INTO `kota` VALUES ('336', 'Kabupaten Seruyan');
INSERT INTO `kota` VALUES ('337', 'Kabupaten Katingan');
INSERT INTO `kota` VALUES ('338', 'Kabupaten Pulang Pisau');
INSERT INTO `kota` VALUES ('339', 'Kabupaten Gunung Mas');
INSERT INTO `kota` VALUES ('340', 'Kabupaten Barito Timur');
INSERT INTO `kota` VALUES ('341', 'Kabupaten Murung Raya');
INSERT INTO `kota` VALUES ('342', 'Kota Palangka Raya');
INSERT INTO `kota` VALUES ('343', 'Kabupaten Tanah Laut');
INSERT INTO `kota` VALUES ('344', 'Kabupaten Kota Baru');
INSERT INTO `kota` VALUES ('345', 'Kabupaten Banjar');
INSERT INTO `kota` VALUES ('346', 'Kabupaten Barito Kuala');
INSERT INTO `kota` VALUES ('347', 'Kabupaten Tapin');
INSERT INTO `kota` VALUES ('348', 'Kabupaten Hulu Sungai Selatan');
INSERT INTO `kota` VALUES ('349', 'Kabupaten Hulu Sungai Tengah');
INSERT INTO `kota` VALUES ('350', 'Kabupaten Hulu Sungai Utara');
INSERT INTO `kota` VALUES ('351', 'Kabupaten Tabalong');
INSERT INTO `kota` VALUES ('352', 'Kabupaten Tanah Bumbu');
INSERT INTO `kota` VALUES ('353', 'Kabupaten Balangan');
INSERT INTO `kota` VALUES ('354', 'Kota Banjarmasin');
INSERT INTO `kota` VALUES ('355', 'Kota Banjar Baru');
INSERT INTO `kota` VALUES ('356', 'Kabupaten Paser');
INSERT INTO `kota` VALUES ('357', 'Kabupaten Kutai Barat');
INSERT INTO `kota` VALUES ('358', 'Kabupaten Kutai Kartanegara');
INSERT INTO `kota` VALUES ('359', 'Kabupaten Kutai Timur');
INSERT INTO `kota` VALUES ('360', 'Kabupaten Berau');
INSERT INTO `kota` VALUES ('361', 'Kabupaten Penajam Paser Utara');
INSERT INTO `kota` VALUES ('362', 'Kabupaten Mahakam Hulu');
INSERT INTO `kota` VALUES ('363', 'Kota Balikpapan');
INSERT INTO `kota` VALUES ('364', 'Kota Samarinda');
INSERT INTO `kota` VALUES ('365', 'Kota Bontang');
INSERT INTO `kota` VALUES ('366', 'Kabupaten Malinau');
INSERT INTO `kota` VALUES ('367', 'Kabupaten Bulungan');
INSERT INTO `kota` VALUES ('368', 'Kabupaten Tana Tidung');
INSERT INTO `kota` VALUES ('369', 'Kabupaten Nunukan');
INSERT INTO `kota` VALUES ('370', 'Kota Tarakan');
INSERT INTO `kota` VALUES ('371', 'Kabupaten Bolaang Mongondow');
INSERT INTO `kota` VALUES ('372', 'Kabupaten Minahasa');
INSERT INTO `kota` VALUES ('373', 'Kabupaten Kepulauan Sangihe');
INSERT INTO `kota` VALUES ('374', 'Kabupaten Kepulauan Talaud');
INSERT INTO `kota` VALUES ('375', 'Kabupaten Minahasa Selatan');
INSERT INTO `kota` VALUES ('376', 'Kabupaten Minahasa Utara');
INSERT INTO `kota` VALUES ('377', 'Kabupaten Bolaang Mongondow Utara');
INSERT INTO `kota` VALUES ('378', 'Kabupaten Siau Tagulandang Biaro');
INSERT INTO `kota` VALUES ('379', 'Kabupaten Minahasa Tenggara');
INSERT INTO `kota` VALUES ('380', 'Kabupaten Bolaang Mongondow Selatan');
INSERT INTO `kota` VALUES ('381', 'Kabupaten Bolaang Mongondow Timur');
INSERT INTO `kota` VALUES ('382', 'Kota Manado');
INSERT INTO `kota` VALUES ('383', 'Kota Bitung');
INSERT INTO `kota` VALUES ('384', 'Kota Tomohon');
INSERT INTO `kota` VALUES ('385', 'Kota Kotamobagu');
INSERT INTO `kota` VALUES ('386', 'Kabupaten Banggai Kepulauan');
INSERT INTO `kota` VALUES ('387', 'Kabupaten Banggai');
INSERT INTO `kota` VALUES ('388', 'Kabupaten Morowali');
INSERT INTO `kota` VALUES ('389', 'Kabupaten Poso');
INSERT INTO `kota` VALUES ('390', 'Kabupaten Donggala');
INSERT INTO `kota` VALUES ('391', 'Kabupaten Toli-Toli');
INSERT INTO `kota` VALUES ('392', 'Kabupaten Buol');
INSERT INTO `kota` VALUES ('393', 'Kabupaten Parigi Moutong');
INSERT INTO `kota` VALUES ('394', 'Kabupaten Tojo Una-Una');
INSERT INTO `kota` VALUES ('395', 'Kabupaten Sigi');
INSERT INTO `kota` VALUES ('396', 'Kabupaten Banggai Laut');
INSERT INTO `kota` VALUES ('397', 'Kabupaten Morowali Utara');
INSERT INTO `kota` VALUES ('398', 'Kota Palu');
INSERT INTO `kota` VALUES ('399', 'Kabupaten Kepulauan Selayar');
INSERT INTO `kota` VALUES ('400', 'Kabupaten Bulukumba');
INSERT INTO `kota` VALUES ('401', 'Kabupaten Bantaeng');
INSERT INTO `kota` VALUES ('402', 'Kabupaten Jeneponto');
INSERT INTO `kota` VALUES ('403', 'Kabupaten Takalar');
INSERT INTO `kota` VALUES ('404', 'Kabupaten Gowa');
INSERT INTO `kota` VALUES ('405', 'Kabupaten Sinjai');
INSERT INTO `kota` VALUES ('406', 'Kabupaten Maros');
INSERT INTO `kota` VALUES ('407', 'Kabupaten Pangkajene Dan Kepulauan');
INSERT INTO `kota` VALUES ('408', 'Kabupaten Barru');
INSERT INTO `kota` VALUES ('409', 'Kabupaten Bone');
INSERT INTO `kota` VALUES ('410', 'Kabupaten Soppeng');
INSERT INTO `kota` VALUES ('411', 'Kabupaten Wajo');
INSERT INTO `kota` VALUES ('412', 'Kabupaten Sidenreng Rappang');
INSERT INTO `kota` VALUES ('413', 'Kabupaten Pinrang');
INSERT INTO `kota` VALUES ('414', 'Kabupaten Enrekang');
INSERT INTO `kota` VALUES ('415', 'Kabupaten Luwu');
INSERT INTO `kota` VALUES ('416', 'Kabupaten Tana Toraja');
INSERT INTO `kota` VALUES ('417', 'Kabupaten Luwu Utara');
INSERT INTO `kota` VALUES ('418', 'Kabupaten Luwu Timur');
INSERT INTO `kota` VALUES ('419', 'Kabupaten Toraja Utara');
INSERT INTO `kota` VALUES ('420', 'Kota Makassar');
INSERT INTO `kota` VALUES ('421', 'Kota Parepare');
INSERT INTO `kota` VALUES ('422', 'Kota Palopo');
INSERT INTO `kota` VALUES ('423', 'Kabupaten Buton');
INSERT INTO `kota` VALUES ('424', 'Kabupaten Muna');
INSERT INTO `kota` VALUES ('425', 'Kabupaten Konawe');
INSERT INTO `kota` VALUES ('426', 'Kabupaten Kolaka');
INSERT INTO `kota` VALUES ('427', 'Kabupaten Konawe Selatan');
INSERT INTO `kota` VALUES ('428', 'Kabupaten Bombana');
INSERT INTO `kota` VALUES ('429', 'Kabupaten Wakatobi');
INSERT INTO `kota` VALUES ('430', 'Kabupaten Kolaka Utara');
INSERT INTO `kota` VALUES ('431', 'Kabupaten Buton Utara');
INSERT INTO `kota` VALUES ('432', 'Kabupaten Konawe Utara');
INSERT INTO `kota` VALUES ('433', 'Kabupaten Kolaka Timur');
INSERT INTO `kota` VALUES ('434', 'Kabupaten Konawe Kepulauan');
INSERT INTO `kota` VALUES ('435', 'Kabupaten Muna Barat');
INSERT INTO `kota` VALUES ('436', 'Kabupaten Buton Tengah');
INSERT INTO `kota` VALUES ('437', 'Kabupaten Buton Selatan');
INSERT INTO `kota` VALUES ('438', 'Kota Kendari');
INSERT INTO `kota` VALUES ('439', 'Kota Baubau');
INSERT INTO `kota` VALUES ('440', 'Kabupaten Boalemo');
INSERT INTO `kota` VALUES ('441', 'Kabupaten Gorontalo');
INSERT INTO `kota` VALUES ('442', 'Kabupaten Pohuwato');
INSERT INTO `kota` VALUES ('443', 'Kabupaten Bone Bolango');
INSERT INTO `kota` VALUES ('444', 'Kabupaten Gorontalo Utara');
INSERT INTO `kota` VALUES ('445', 'Kota Gorontalo');
INSERT INTO `kota` VALUES ('446', 'Kabupaten Majene');
INSERT INTO `kota` VALUES ('447', 'Kabupaten Polewali Mandar');
INSERT INTO `kota` VALUES ('448', 'Kabupaten Mamasa');
INSERT INTO `kota` VALUES ('449', 'Kabupaten Mamuju');
INSERT INTO `kota` VALUES ('450', 'Kabupaten Mamuju Utara');
INSERT INTO `kota` VALUES ('451', 'Kabupaten Mamuju Tengah');
INSERT INTO `kota` VALUES ('452', 'Kabupaten Maluku Tenggara Barat');
INSERT INTO `kota` VALUES ('453', 'Kabupaten Maluku Tenggara');
INSERT INTO `kota` VALUES ('454', 'Kabupaten Maluku Tengah');
INSERT INTO `kota` VALUES ('455', 'Kabupaten Buru');
INSERT INTO `kota` VALUES ('456', 'Kabupaten Kepulauan Aru');
INSERT INTO `kota` VALUES ('457', 'Kabupaten Seram Bagian Barat');
INSERT INTO `kota` VALUES ('458', 'Kabupaten Seram Bagian Timur');
INSERT INTO `kota` VALUES ('459', 'Kabupaten Maluku Barat Daya');
INSERT INTO `kota` VALUES ('460', 'Kabupaten Buru Selatan');
INSERT INTO `kota` VALUES ('461', 'Kota Ambon');
INSERT INTO `kota` VALUES ('462', 'Kota Tual');
INSERT INTO `kota` VALUES ('463', 'Kabupaten Halmahera Barat');
INSERT INTO `kota` VALUES ('464', 'Kabupaten Halmahera Tengah');
INSERT INTO `kota` VALUES ('465', 'Kabupaten Kepulauan Sula');
INSERT INTO `kota` VALUES ('466', 'Kabupaten Halmahera Selatan');
INSERT INTO `kota` VALUES ('467', 'Kabupaten Halmahera Utara');
INSERT INTO `kota` VALUES ('468', 'Kabupaten Halmahera Timur');
INSERT INTO `kota` VALUES ('469', 'Kabupaten Pulau Morotai');
INSERT INTO `kota` VALUES ('470', 'Kabupaten Pulau Taliabu');
INSERT INTO `kota` VALUES ('471', 'Kota Ternate');
INSERT INTO `kota` VALUES ('472', 'Kota Tidore Kepulauan');
INSERT INTO `kota` VALUES ('473', 'Kabupaten Fakfak');
INSERT INTO `kota` VALUES ('474', 'Kabupaten Kaimana');
INSERT INTO `kota` VALUES ('475', 'Kabupaten Teluk Wondama');
INSERT INTO `kota` VALUES ('476', 'Kabupaten Teluk Bintuni');
INSERT INTO `kota` VALUES ('477', 'Kabupaten Manokwari');
INSERT INTO `kota` VALUES ('478', 'Kabupaten Sorong Selatan');
INSERT INTO `kota` VALUES ('479', 'Kabupaten Sorong');
INSERT INTO `kota` VALUES ('480', 'Kabupaten Raja Ampat');
INSERT INTO `kota` VALUES ('481', 'Kabupaten Tambrauw');
INSERT INTO `kota` VALUES ('482', 'Kabupaten Maybrat');
INSERT INTO `kota` VALUES ('483', 'Kabupaten Manokwari Selatan');
INSERT INTO `kota` VALUES ('484', 'Kabupaten Pegunungan Arfak');
INSERT INTO `kota` VALUES ('485', 'Kota Sorong');
INSERT INTO `kota` VALUES ('486', 'Kabupaten Merauke');
INSERT INTO `kota` VALUES ('487', 'Kabupaten Jayawijaya');
INSERT INTO `kota` VALUES ('488', 'Kabupaten Jayapura');
INSERT INTO `kota` VALUES ('489', 'Kabupaten Nabire');
INSERT INTO `kota` VALUES ('490', 'Kabupaten Kepulauan Yapen');
INSERT INTO `kota` VALUES ('491', 'Kabupaten Biak Numfor');
INSERT INTO `kota` VALUES ('492', 'Kabupaten Paniai');
INSERT INTO `kota` VALUES ('493', 'Kabupaten Puncak Jaya');
INSERT INTO `kota` VALUES ('494', 'Kabupaten Mimika');
INSERT INTO `kota` VALUES ('495', 'Kabupaten Boven Digoel');
INSERT INTO `kota` VALUES ('496', 'Kabupaten Mappi');
INSERT INTO `kota` VALUES ('497', 'Kabupaten Asmat');
INSERT INTO `kota` VALUES ('498', 'Kabupaten Yahukimo');
INSERT INTO `kota` VALUES ('499', 'Kabupaten Pegunungan Bintang');
INSERT INTO `kota` VALUES ('500', 'Kabupaten Tolikara');
INSERT INTO `kota` VALUES ('501', 'Kabupaten Sarmi');
INSERT INTO `kota` VALUES ('502', 'Kabupaten Keerom');
INSERT INTO `kota` VALUES ('503', 'Kabupaten Waropen');
INSERT INTO `kota` VALUES ('504', 'Kabupaten Supiori');
INSERT INTO `kota` VALUES ('505', 'Kabupaten Mamberamo Raya');
INSERT INTO `kota` VALUES ('506', 'Kabupaten Nduga');
INSERT INTO `kota` VALUES ('507', 'Kabupaten Lanny Jaya');
INSERT INTO `kota` VALUES ('508', 'Kabupaten Mamberamo Tengah');
INSERT INTO `kota` VALUES ('509', 'Kabupaten Yalimo');
INSERT INTO `kota` VALUES ('510', 'Kabupaten Puncak');
INSERT INTO `kota` VALUES ('511', 'Kabupaten Dogiyai');
INSERT INTO `kota` VALUES ('512', 'Kabupaten Intan Jaya');
INSERT INTO `kota` VALUES ('513', 'Kabupaten Deiyai');
INSERT INTO `kota` VALUES ('514', 'Kota Jayapura');

-- ----------------------------
-- Table structure for negara
-- ----------------------------
DROP TABLE IF EXISTS `negara`;
CREATE TABLE `negara` (
  `id_negara` int(11) NOT NULL AUTO_INCREMENT,
  `nama_negara` varchar(255) NOT NULL,
  PRIMARY KEY (`id_negara`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of negara
-- ----------------------------
INSERT INTO `negara` VALUES ('1', 'Afghanistan');
INSERT INTO `negara` VALUES ('2', 'Albania');
INSERT INTO `negara` VALUES ('3', 'Algeria');
INSERT INTO `negara` VALUES ('4', 'American Samoa');
INSERT INTO `negara` VALUES ('5', 'Andorra');
INSERT INTO `negara` VALUES ('6', 'Angola');
INSERT INTO `negara` VALUES ('7', 'Anguilla');
INSERT INTO `negara` VALUES ('8', 'Antarctica');
INSERT INTO `negara` VALUES ('9', 'Antigua and Barbuda');
INSERT INTO `negara` VALUES ('10', 'Argentina');
INSERT INTO `negara` VALUES ('11', 'Armenia');
INSERT INTO `negara` VALUES ('12', 'Aruba');
INSERT INTO `negara` VALUES ('13', 'Australia');
INSERT INTO `negara` VALUES ('14', 'Austria');
INSERT INTO `negara` VALUES ('15', 'Azerbaijan');
INSERT INTO `negara` VALUES ('16', 'Bahamas');
INSERT INTO `negara` VALUES ('17', 'Bahrain');
INSERT INTO `negara` VALUES ('18', 'Bangladesh');
INSERT INTO `negara` VALUES ('19', 'Barbados');
INSERT INTO `negara` VALUES ('20', 'Belarus');
INSERT INTO `negara` VALUES ('21', 'Belgium');
INSERT INTO `negara` VALUES ('22', 'Belize');
INSERT INTO `negara` VALUES ('23', 'Benin');
INSERT INTO `negara` VALUES ('24', 'Bermuda');
INSERT INTO `negara` VALUES ('25', 'Bhutan');
INSERT INTO `negara` VALUES ('26', 'Bolivia');
INSERT INTO `negara` VALUES ('27', 'Bosnia and Herzegovina');
INSERT INTO `negara` VALUES ('28', 'Botswana');
INSERT INTO `negara` VALUES ('29', 'Bouvet Island');
INSERT INTO `negara` VALUES ('30', 'Brazil');
INSERT INTO `negara` VALUES ('31', 'British Indian Ocean Territory');
INSERT INTO `negara` VALUES ('32', 'Brunei Darussalam');
INSERT INTO `negara` VALUES ('33', 'Bulgaria');
INSERT INTO `negara` VALUES ('34', 'Burkina Faso');
INSERT INTO `negara` VALUES ('35', 'Burundi');
INSERT INTO `negara` VALUES ('36', 'Cambodia');
INSERT INTO `negara` VALUES ('37', 'Cameroon');
INSERT INTO `negara` VALUES ('38', 'Canada');
INSERT INTO `negara` VALUES ('39', 'Cape Verde');
INSERT INTO `negara` VALUES ('40', 'Cayman Islands');
INSERT INTO `negara` VALUES ('41', 'Central African Republic');
INSERT INTO `negara` VALUES ('42', 'Chad');
INSERT INTO `negara` VALUES ('43', 'Chile');
INSERT INTO `negara` VALUES ('44', 'China');
INSERT INTO `negara` VALUES ('45', 'Christmas Island');
INSERT INTO `negara` VALUES ('46', 'Cocos (Keeling) Islands');
INSERT INTO `negara` VALUES ('47', 'Colombia');
INSERT INTO `negara` VALUES ('48', 'Comoros');
INSERT INTO `negara` VALUES ('49', 'Congo');
INSERT INTO `negara` VALUES ('50', 'Congo, the Democratic Republic of the');
INSERT INTO `negara` VALUES ('51', 'Cook Islands');
INSERT INTO `negara` VALUES ('52', 'Costa Rica');
INSERT INTO `negara` VALUES ('53', 'Cote D\'Ivoire');
INSERT INTO `negara` VALUES ('54', 'Croatia');
INSERT INTO `negara` VALUES ('55', 'Cuba');
INSERT INTO `negara` VALUES ('56', 'Cyprus');
INSERT INTO `negara` VALUES ('57', 'Czech Republic');
INSERT INTO `negara` VALUES ('58', 'Denmark');
INSERT INTO `negara` VALUES ('59', 'Djibouti');
INSERT INTO `negara` VALUES ('60', 'Dominica');
INSERT INTO `negara` VALUES ('61', 'Dominican Republic');
INSERT INTO `negara` VALUES ('62', 'Ecuador');
INSERT INTO `negara` VALUES ('63', 'Egypt');
INSERT INTO `negara` VALUES ('64', 'El Salvador');
INSERT INTO `negara` VALUES ('65', 'Equatorial Guinea');
INSERT INTO `negara` VALUES ('66', 'Eritrea');
INSERT INTO `negara` VALUES ('67', 'Estonia');
INSERT INTO `negara` VALUES ('68', 'Ethiopia');
INSERT INTO `negara` VALUES ('69', 'Falkland Islands (Malvinas)');
INSERT INTO `negara` VALUES ('70', 'Faroe Islands');
INSERT INTO `negara` VALUES ('71', 'Fiji');
INSERT INTO `negara` VALUES ('72', 'Finland');
INSERT INTO `negara` VALUES ('73', 'France');
INSERT INTO `negara` VALUES ('74', 'French Guiana');
INSERT INTO `negara` VALUES ('75', 'French Polynesia');
INSERT INTO `negara` VALUES ('76', 'French Southern Territories');
INSERT INTO `negara` VALUES ('77', 'Gabon');
INSERT INTO `negara` VALUES ('78', 'Gambia');
INSERT INTO `negara` VALUES ('79', 'Georgia');
INSERT INTO `negara` VALUES ('80', 'Germany');
INSERT INTO `negara` VALUES ('81', 'Ghana');
INSERT INTO `negara` VALUES ('82', 'Gibraltar');
INSERT INTO `negara` VALUES ('83', 'Greece');
INSERT INTO `negara` VALUES ('84', 'Greenland');
INSERT INTO `negara` VALUES ('85', 'Grenada');
INSERT INTO `negara` VALUES ('86', 'Guadeloupe');
INSERT INTO `negara` VALUES ('87', 'Guam');
INSERT INTO `negara` VALUES ('88', 'Guatemala');
INSERT INTO `negara` VALUES ('89', 'Guinea');
INSERT INTO `negara` VALUES ('90', 'Guinea-Bissau');
INSERT INTO `negara` VALUES ('91', 'Guyana');
INSERT INTO `negara` VALUES ('92', 'Haiti');
INSERT INTO `negara` VALUES ('93', 'Heard Island and Mcdonald Islands');
INSERT INTO `negara` VALUES ('94', 'Holy See (Vatican City State)');
INSERT INTO `negara` VALUES ('95', 'Honduras');
INSERT INTO `negara` VALUES ('96', 'Hong Kong');
INSERT INTO `negara` VALUES ('97', 'Hungary');
INSERT INTO `negara` VALUES ('98', 'Iceland');
INSERT INTO `negara` VALUES ('99', 'India');
INSERT INTO `negara` VALUES ('100', 'Indonesia');
INSERT INTO `negara` VALUES ('101', 'Iran, Islamic Republic of');
INSERT INTO `negara` VALUES ('102', 'Iraq');
INSERT INTO `negara` VALUES ('103', 'Ireland');
INSERT INTO `negara` VALUES ('104', 'Israel');
INSERT INTO `negara` VALUES ('105', 'Italy');
INSERT INTO `negara` VALUES ('106', 'Jamaica');
INSERT INTO `negara` VALUES ('107', 'Japan');
INSERT INTO `negara` VALUES ('108', 'Jordan');
INSERT INTO `negara` VALUES ('109', 'Kazakhstan');
INSERT INTO `negara` VALUES ('110', 'Kenya');
INSERT INTO `negara` VALUES ('111', 'Kiribati');
INSERT INTO `negara` VALUES ('112', 'Korea, Democratic People\'s Republic of');
INSERT INTO `negara` VALUES ('113', 'Korea, Republic of');
INSERT INTO `negara` VALUES ('114', 'Kuwait');
INSERT INTO `negara` VALUES ('115', 'Kyrgyzstan');
INSERT INTO `negara` VALUES ('116', 'Lao People\'s Democratic Republic');
INSERT INTO `negara` VALUES ('117', 'Latvia');
INSERT INTO `negara` VALUES ('118', 'Lebanon');
INSERT INTO `negara` VALUES ('119', 'Lesotho');
INSERT INTO `negara` VALUES ('120', 'Liberia');
INSERT INTO `negara` VALUES ('121', 'Libyan Arab Jamahiriya');
INSERT INTO `negara` VALUES ('122', 'Liechtenstein');
INSERT INTO `negara` VALUES ('123', 'Lithuania');
INSERT INTO `negara` VALUES ('124', 'Luxembourg');
INSERT INTO `negara` VALUES ('125', 'Macao');
INSERT INTO `negara` VALUES ('126', 'Macedonia, the Former Yugoslav Republic of');
INSERT INTO `negara` VALUES ('127', 'Madagascar');
INSERT INTO `negara` VALUES ('128', 'Malawi');
INSERT INTO `negara` VALUES ('129', 'Malaysia');
INSERT INTO `negara` VALUES ('130', 'Maldives');
INSERT INTO `negara` VALUES ('131', 'Mali');
INSERT INTO `negara` VALUES ('132', 'Malta');
INSERT INTO `negara` VALUES ('133', 'Marshall Islands');
INSERT INTO `negara` VALUES ('134', 'Martinique');
INSERT INTO `negara` VALUES ('135', 'Mauritania');
INSERT INTO `negara` VALUES ('136', 'Mauritius');
INSERT INTO `negara` VALUES ('137', 'Mayotte');
INSERT INTO `negara` VALUES ('138', 'Mexico');
INSERT INTO `negara` VALUES ('139', 'Micronesia, Federated States of');
INSERT INTO `negara` VALUES ('140', 'Moldova, Republic of');
INSERT INTO `negara` VALUES ('141', 'Monaco');
INSERT INTO `negara` VALUES ('142', 'Mongolia');
INSERT INTO `negara` VALUES ('143', 'Montserrat');
INSERT INTO `negara` VALUES ('144', 'Morocco');
INSERT INTO `negara` VALUES ('145', 'Mozambique');
INSERT INTO `negara` VALUES ('146', 'Myanmar');
INSERT INTO `negara` VALUES ('147', 'Namibia');
INSERT INTO `negara` VALUES ('148', 'Nauru');
INSERT INTO `negara` VALUES ('149', 'Nepal');
INSERT INTO `negara` VALUES ('150', 'Netherlands');
INSERT INTO `negara` VALUES ('151', 'Netherlands Antilles');
INSERT INTO `negara` VALUES ('152', 'New Caledonia');
INSERT INTO `negara` VALUES ('153', 'New Zealand');
INSERT INTO `negara` VALUES ('154', 'Nicaragua');
INSERT INTO `negara` VALUES ('155', 'Niger');
INSERT INTO `negara` VALUES ('156', 'Nigeria');
INSERT INTO `negara` VALUES ('157', 'Niue');
INSERT INTO `negara` VALUES ('158', 'Norfolk Island');
INSERT INTO `negara` VALUES ('159', 'Northern Mariana Islands');
INSERT INTO `negara` VALUES ('160', 'Norway');
INSERT INTO `negara` VALUES ('161', 'Oman');
INSERT INTO `negara` VALUES ('162', 'Pakistan');
INSERT INTO `negara` VALUES ('163', 'Palau');
INSERT INTO `negara` VALUES ('164', 'Palestinian Territory, Occupied');
INSERT INTO `negara` VALUES ('165', 'Panama');
INSERT INTO `negara` VALUES ('166', 'Papua New Guinea');
INSERT INTO `negara` VALUES ('167', 'Paraguay');
INSERT INTO `negara` VALUES ('168', 'Peru');
INSERT INTO `negara` VALUES ('169', 'Philippines');
INSERT INTO `negara` VALUES ('170', 'Pitcairn');
INSERT INTO `negara` VALUES ('171', 'Poland');
INSERT INTO `negara` VALUES ('172', 'Portugal');
INSERT INTO `negara` VALUES ('173', 'Puerto Rico');
INSERT INTO `negara` VALUES ('174', 'Qatar');
INSERT INTO `negara` VALUES ('175', 'Reunion');
INSERT INTO `negara` VALUES ('176', 'Romania');
INSERT INTO `negara` VALUES ('177', 'Russian Federation');
INSERT INTO `negara` VALUES ('178', 'Rwanda');
INSERT INTO `negara` VALUES ('179', 'Saint Helena');
INSERT INTO `negara` VALUES ('180', 'Saint Kitts and Nevis');
INSERT INTO `negara` VALUES ('181', 'Saint Lucia');
INSERT INTO `negara` VALUES ('182', 'Saint Pierre and Miquelon');
INSERT INTO `negara` VALUES ('183', 'Saint Vincent and the Grenadines');
INSERT INTO `negara` VALUES ('184', 'Samoa');
INSERT INTO `negara` VALUES ('185', 'San Marino');
INSERT INTO `negara` VALUES ('186', 'Sao Tome and Principe');
INSERT INTO `negara` VALUES ('187', 'Saudi Arabia');
INSERT INTO `negara` VALUES ('188', 'Senegal');
INSERT INTO `negara` VALUES ('189', 'Serbia and Montenegro');
INSERT INTO `negara` VALUES ('190', 'Seychelles');
INSERT INTO `negara` VALUES ('191', 'Sierra Leone');
INSERT INTO `negara` VALUES ('192', 'Singapore');
INSERT INTO `negara` VALUES ('193', 'Slovakia');
INSERT INTO `negara` VALUES ('194', 'Slovenia');
INSERT INTO `negara` VALUES ('195', 'Solomon Islands');
INSERT INTO `negara` VALUES ('196', 'Somalia');
INSERT INTO `negara` VALUES ('197', 'South Africa');
INSERT INTO `negara` VALUES ('198', 'South Georgia and the South Sandwich Islands');
INSERT INTO `negara` VALUES ('199', 'Spain');
INSERT INTO `negara` VALUES ('200', 'Sri Lanka');
INSERT INTO `negara` VALUES ('201', 'Sudan');
INSERT INTO `negara` VALUES ('202', 'Suriname');
INSERT INTO `negara` VALUES ('203', 'Svalbard and Jan Mayen');
INSERT INTO `negara` VALUES ('204', 'Swaziland');
INSERT INTO `negara` VALUES ('205', 'Sweden');
INSERT INTO `negara` VALUES ('206', 'Switzerland');
INSERT INTO `negara` VALUES ('207', 'Syrian Arab Republic');
INSERT INTO `negara` VALUES ('208', 'Taiwan, Province of China');
INSERT INTO `negara` VALUES ('209', 'Tajikistan');
INSERT INTO `negara` VALUES ('210', 'Tanzania, United Republic of');
INSERT INTO `negara` VALUES ('211', 'Thailand');
INSERT INTO `negara` VALUES ('212', 'Timor-Leste');
INSERT INTO `negara` VALUES ('213', 'Togo');
INSERT INTO `negara` VALUES ('214', 'Tokelau');
INSERT INTO `negara` VALUES ('215', 'Tonga');
INSERT INTO `negara` VALUES ('216', 'Trinidad and Tobago');
INSERT INTO `negara` VALUES ('217', 'Tunisia');
INSERT INTO `negara` VALUES ('218', 'Turkey');
INSERT INTO `negara` VALUES ('219', 'Turkmenistan');
INSERT INTO `negara` VALUES ('220', 'Turks and Caicos Islands');
INSERT INTO `negara` VALUES ('221', 'Tuvalu');
INSERT INTO `negara` VALUES ('222', 'Uganda');
INSERT INTO `negara` VALUES ('223', 'Ukraine');
INSERT INTO `negara` VALUES ('224', 'United Arab Emirates');
INSERT INTO `negara` VALUES ('225', 'United Kingdom');
INSERT INTO `negara` VALUES ('226', 'United States');
INSERT INTO `negara` VALUES ('227', 'United States Minor Outlying Islands');
INSERT INTO `negara` VALUES ('228', 'Uruguay');
INSERT INTO `negara` VALUES ('229', 'Uzbekistan');
INSERT INTO `negara` VALUES ('230', 'Vanuatu');
INSERT INTO `negara` VALUES ('231', 'Venezuela');
INSERT INTO `negara` VALUES ('232', 'Viet Nam');
INSERT INTO `negara` VALUES ('233', 'Virgin Islands, British');
INSERT INTO `negara` VALUES ('234', 'Virgin Islands, U.S.');
INSERT INTO `negara` VALUES ('235', 'Wallis and Futuna');
INSERT INTO `negara` VALUES ('236', 'Western Sahara');
INSERT INTO `negara` VALUES ('237', 'Yemen');
INSERT INTO `negara` VALUES ('238', 'Zambia');
INSERT INTO `negara` VALUES ('239', 'Zimbabwe');

-- ----------------------------
-- Table structure for pegawai
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` bigint(20) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pegawai`),
  UNIQUE KEY `nip` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES ('1', '198208302006041001', 'Nyoman Suryadi');
INSERT INTO `pegawai` VALUES ('2', '199205262010121001', 'Erda Nugraha');

-- ----------------------------
-- Table structure for pelabuhan
-- ----------------------------
DROP TABLE IF EXISTS `pelabuhan`;
CREATE TABLE `pelabuhan` (
  `id_pelabuhan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelabuhan` varchar(255) NOT NULL,
  `id_kota` int(11) NOT NULL,
  PRIMARY KEY (`id_pelabuhan`),
  KEY `id_kota` (`id_kota`),
  CONSTRAINT `pelabuhan_ibfk_1` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelabuhan
-- ----------------------------
INSERT INTO `pelabuhan` VALUES ('1', 'Muarajati', '182');
INSERT INTO `pelabuhan` VALUES ('2', 'Balongan', '172');
